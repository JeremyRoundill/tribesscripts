// Dict.cs

function Dict::Reset(%dict) {
	deleteVariables("$Dict::"@%dict@"_*");
}

function Dict::New(%dict) {
	Dict::Reset(%dict);
	$Dict::[%dict, count] = -1;
	$Dict::[%dict, keymap] = -1;
	$Dict::[%dict, keys] = -1;
	$Dict::[%dict, values] = -1;
}

function Dict::GetKeyPosition(%dict, %key) {
	if($Dict::[%dict, keymap] == "") {
		return -1;
	}
	return $Dict::[%dict, keymap, %key];
}

function Dict::GetByKey(%dict, %key) {
	%position = Dict::GetKeyPosition(%dict, %key);
	if(%position == "" || %position == -1) {
		return "";
	}
	return $Dict::[%dict, values, %position];
}

function Dict::SetByKey(%dict, %key, %value) {
	// Find where element is in value array
	%position = Dict::GetKeyPosition(%dict, %key);
	
	// Var representing whether or not we found a match for the supplied key
	%found = true;
	
	// If we didn't find a matching key, increment the count of the dict, and
	// set the position to the new count (so we're inserting at the end of
	// the values array
	if(%position == "" || %position == -1) {
		$Dict::[%dict, count]++;
		%position = $Dict::[%dict, count];
		%found = false;
	}
	
	if(%value != "" && %value != -1) {
		// If the value isn't null, insert it.
		$Dict::[%dict, keymap, %key] = %position;
		$Dict::[%dict, keys, %position] = %key;
		$Dict::[%dict, values, %position] = %value;
	} else {
		// If the value is null, clear it
		$Dict::[%dict, keymap, %key] = "";
		$Dict::[%dict, keys, %position] = "";
		$Dict::[%dict, values, %position] = "";
		// If we found a matching key earlier, decrement the count because we're
		// deleting a key and a value

		if(%found) {
			for(%i = %position + 1; %i < $Dict::[%dict, count]; %i++) {
				%key = $Dict::[%dict, keys, %i];
				%value = $Dict::[%dict, values, %i];

				$Dict::[%dict, keymap, %key] = %i - 1;
				$Dict::[%dict, keys, %i - 1] = %key;
				$Dict::[%dict, values, %i - 1] = %value;

			}
			$Dict::[%dict, count]--;
		}
	}
	// Return the length of the dict
	return Dict::Count(%dict);	
}

function Dict::Key(%dict, %index) {
	return $Dict::[%dict, keys, %index];
}

function Dict::Count(%dict) {
	return $Dict::[%dict, count] + 1;
}

function Dict::Print(%dict) {
	for(%i = 0; $Dict::[%dict, keys, %i] != ""; %i++) {
		echo(%i @ "k: " @ $Dict::[%dict, keys, %i]);
		echo(%i @ "v: " @ $Dict::[%dict, values, %i]);
		echo("--");
	}
}


// Dict::New(test);


// Dict::GetByKey(test, lemon);
// Dict::SetByKey(test, lemon, lime);
// echo("should be lime: " @ Dict::GetByKey(test, lemon));
// echo("should return nothing: " @ Dict::GetByKey(test, woop));
//Dict::GetByKey(test, woop);
// Dict::SetByKey(test, lemon);
// Dict::GetByKey(test, lemon);
// Dict::SetByKey(test, woop, woop1);
// Dict::GetByKey(test, woop);
// Dict::SetByKey(test, woop, woop2);
// Dict::GetByKey(test, woop);
// Dict::SetByKey(test, apple, orange);
// Dict::GetByKey(test, apple);
// Dict::SetByKey(test, banana, peach);
// Dict::GetByKey(test, banana);

// echo("should return nothing: " @ Dict::GetByKey(test, lemon));
// echo(Dict::SetByKey(test, lemon, lime));
// echo("should return lime: " @ Dict::GetByKey(test, lemon));
// echo("should return nothing: " @ Dict::GetByKey(test, woop));
// echo(Dict::SetByKey(test, lemon));
// echo("should return nothing: " @ Dict::GetByKey(test, lemon));
// echo(Dict::SetByKey(test, woop, woop1));
// echo("should return woop1: " @ Dict::GetByKey(test, woop));
// echo(Dict::SetByKey(test, woop, woop2));
// echo("should return woop2: " @ Dict::GetByKey(test, woop));
// echo(Dict::SetByKey(test, apple, orange));
// echo("should return orange: " @ Dict::GetByKey(test, apple));
// echo(Dict::SetByKey(test, banana, peach));
// echo("should return peach: " @ Dict::GetByKey(test, banana));
