// SP.cs

// Constants
// Positions of skills in menu
$SP::Position["Slashing"] = 1;
$SP::Position["Piercing"] = 2;
$SP::Position["Bludgeoning"] = 3;
$SP::Position["Dodging"] = 4;
$SP::Position["Weight Capacity"] = 5;
$SP::Position["Bashing"] = 6;
$SP::Position["Stealing"] = 7;
$SP::Position["Hiding"] = 8;
$SP::Position["Backstabbing"] = 9;
$SP::Position["Offensive Casting"] = 10;
$SP::Position["Defensive Casting"] = 11;
$SP::Position["Spell Resistance"] = 12;
$SP::Position["Healing"] = 13;
$SP::Position["Archery"] = 14;
$SP::Position["Endurance"] = 15;
$SP::Position["Mining"] = 17;
$SP::Position["Speech"] = 18;
$SP::Position["Sense Heading"] = 19;
$SP::Position["Energy"] = 20;
$SP::Position["Haggling"] = 21;
$SP::Position["Neutral Casting"] = 22;
$SP::Position["Divine Casting"] = 23;
$SP::Position["Cleave"] = 24;

// Variables
// Skills to buy
%i = -1;
$SP::Spend[%i++] = "Neutral Casting";
$SP::Spend[%i++] = "Sense Heading";
$SP::Spend[%i++] = "Offensive Casting";
$SP::Spend[%i++] = "Slashing";
$SP::Spend[%i++] = "Healing";
$SP::Spend[%i++] = "Endurance";
$SP::Spend[%i++] = "Energy";
$SP::Spend[%i++] = "Dodging";
$SP::Spend[%i++] = "Spell Resistance";
$SP::Spend[%i++] = "Weight Capacity";
$SP::Spend[%i++] = "Defensive Casting";

function SP::spend() {
    Message::print("<jc><f0>Spending SP");

    // Open tab menu
    schedule("remoteeval(2048, ScoresOn);", 0);
    // Open SP page
    schedule("ClientMenuSelect(sp);", 1);

    // Loop through SP to spend, and spend them
    for(%i = 0; $SP::Spend[%i] != ""; %i++) {
        schedule("SP::levelUpSkill(\"" @ $SP::Spend[%i] @ "\", 30);", 2 + %i * 2);
    }
    
    // Close tab menu
    schedule("remoteeval(2048, ScoresOff);", 2 + %i++ * 2);
    schedule("Message::print(\"<jc><f0>SP spent!\", 5);", %i++ * 2);
    
    return 5 + %i * 2;
}

function SP::levelUpSkill(%skillName, %amount) {
    Message::print("<jc><f0>Spending <f1>" @ %amount @ "<f0> SP on <f1>" @ %skillName);
    %SPPos = $SP::Position[%skillName];
    // Number of page (there are 6 skills per page, pages start at 1)
    %pageNo = ceil(%SPPos / 6);

    say(1,%amount);
    // Select the SP to spend. ClientMenuSelect takes two arguments separated by a space
    // The first is the option to select, the second is the page it's on
    schedule("ClientMenuSelect(\"" @ %SPPos @ " " @ %pageNo @ "\");", 1);

}
