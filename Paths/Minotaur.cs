// Minotaur.cs


$Paths::Minotaur::pathing = false;

$Paths::Minotaur::menuKey = "n";
$Paths::Minotaur::menuDescription = "Auto walk to Minotaur";
$Paths::Minotaur::menuFunction = "Paths::Minotaur::setup();";

function Paths::Minotaur::stop() {
	$Paths::Minotaur::pathing = false;
	Movement::stop();
	Schedule::Cancel("Movement::splitPath(\"Paths::Minotaur::path2\");");	
	Schedule::Cancel("Paths::JatenBank::NoPack::setup(\"Paths::Minotaur::setup();\");");
}

function Paths::Minotaur::restart() {
	if(!$Paths::Minotaur::pathing) return;
	Paths::Minotaur::stop();
	Paths::Minotaur::setup();
}

function Paths::Minotaur::setup() {
	$Paths::Minotaur::pathing = true;
	Movement::smartTurnCardinal(NW, "Paths::Minotaur::path();");
	Equipment::equip("Boots of Gliding");
}

function Paths::Minotaur::path() {
	if(!$Paths::Minotaur::pathing) return;
	say(2, "#cast transport mino");
	// Movement::initialisePath("Movement::mineRestart();");
	Movement::initialisePath("Paths::Minotaur::restart();");

	// Gotta wait for the teleport to have been completed
	Schedule::Add("Movement::splitPath(\"Paths::Minotaur::path2\");", 5);	
}

function Paths::Minotaur::path2(%direction, %distance) {
	if(!$Paths::Minotaur::pathing) return;

	if(%distance == -1 && %direction == -1) {
		Paths::Minotaur::stop();
		Schedule::Add("Paths::JatenBank::NoPack::setup(\"Paths::Minotaur::setup();\");", 20);
		return;
	}

	if($Movement::CardinalHeading != NW) {
		Movement::queueCardinalTurn(NW);
	}

	if(%distance == 4751) { // works
		Movement::queueCardinalMove(NW, 12);
		Movement::queueCardinalMove(NE, 1.6);
		Movement::queueCardinalMove(NW, 2.5);
	} else if(%distance == 4754 || %distance == 4755) { 
		Movement::queueCardinalMove(NW, 11);
		Movement::queueCardinalMove(NE, 2);
		Movement::queueCardinalMove(NW, 2);
	} else if(%distance == 4757) { // Doesn't seem to go far enough to the right
		Movement::queueCardinalMove(NW, 12);
		Movement::queueCardinalMove(NE, 1.6);
		Movement::queueCardinalMove(NW, 2);
	} else if(%distance == 4759) { // works
		Movement::queueCardinalMove(NW, 12);
		Movement::queueCardinalMove(NE, 2.3);
		Movement::queueCardinalMove(NW, 2);
	} else {
		Paths::Minotaur::restart();
	}

	Movement::queueSanityCheck("North", 4823); // We're in the lair now

	Movement::queueCardinalMove(NW, 0.5);
	Movement::queueCardinalMove(SE, 0.5);
	Movement::queueCardinalMove(S, 1.5);
	Movement::queueCardinalMove(SE, 0.5);
	Movement::queueCardinalMove(S, 0.5);
	Movement::queueCardinalMove(SW, 1);
	Movement::queueCardinalMove(S, 1);

	Movement::queueCardinalMove(E, 1);
	Movement::queueCardinalMove(NE, 2);
	Movement::queueCardinalMove(N, 2);
	Movement::queueCardinalMove(S, 2);
	Movement::queueCardinalMove(W, 1);

	Movement::queueCardinalMove(S, 2);
	Movement::queueCardinalMove(SE, 1);
	Movement::queueCardinalMove(SW, 0.25);
	Movement::queueCardinalMove(SE, 1);
	Movement::queueCardinalMove(NE, 1);
	Movement::queueCardinalMove(E, 1);
	Movement::queueCardinalMove(N, 1);

	Movement::queueSanityCheck("North", 4860);

	Movement::queueCardinalMove(E, 1.5);
	Movement::queueCardinalMove(NW, 0.1);
	Movement::queueCardinalMove(E, 1.5);
	Movement::queueCardinalMove(NW, 0.1);
	Movement::queueCardinalMove(E, 1.5);
	Movement::queueCardinalMove(NW, 0.1);
	Movement::queueCardinalMove(E, 1.5);
	Movement::queueCardinalMove(N, 0.3);
	Movement::queueCardinalMove(NE, 0.3);
	Movement::queueCardinalMove(N, 0.6);
	Movement::queueCardinalMove(S, 0.1);
	Movement::queueCardinalMove(W, 2);
	Movement::queueCardinalMove(SE, 0.1);
	Movement::queueCardinalMove(W, 0.5);
	Movement::queueCardinalMove(N, 2);
	Movement::queueCardinalMove(W, 1);
	Movement::queueCardinalMove(SW, 1.8);
	Movement::queueSanityCheck("North", 4839);

	Movement::queueCardinalMove(NW, 0.4);

	Movement::queueJet(0.8, 5);
	Movement::queueCardinalMove(S, 3);
	Movement::queueCardinalMove(SE, 7);
	Movement::queueLookUpDown(105);

	Movement::queueCallback("Paths::Minotaur::path3();");

	Movement::travelPath();
}

function Paths::Minotaur::path3() {
	if(!$Paths::Minotaur::pathing) return;
	Movement::smartTurnAngle($pi/10, "Paths::Minotaur::path4();");
}

function Paths::Minotaur::path4() {
	if(!$Paths::Minotaur::pathing) return;
	AutoRemort::castBestSpell();
	Actions::startAutoAttack();
}