// GranKar.cs

// NB: This only works from the remort room.
// TODO: Create restart function (i.e. cast #recall and go from recall room)

$Paths::GranKar::pathing = false;

$Paths::GranKar::menuKey = "g";
$Paths::GranKar::menuDescription = "Auto walk to Gran'Kar";
$Paths::GranKar::menuFunction = "Paths::GranKar::setup();";

function Paths::GranKar::stop() {
	$Paths::GranKar::pathing = false;
	Schedule::Cancel("Paths::GranKar::presetup();");
	Movement::stop();
}

function Paths::GranKar::recallAndStart() {
	if(!$Paths::GranKar::pathing) return;
	Paths::GranKar::stop();
	Actions::scheduleAction("recall");
	Schedule::Add("Paths::GranKar::presetup();", 124);
}

function Paths::GranKar::presetup() {
	Movement::smartTurnCardinal(N, "Paths::GranKar::path1();");
	Movement::initialisePath();
	Movement::queueCardinalMove("NW", 5);
	Movement::queueCallback("Paths::GranKar::setup();");
	Movement::travelPath();
}

function Paths::GranKar::setup() {
	$Paths::GranKar::pathing = true;
	Movement::smartTurnCardinal(N, "Paths::GranKar::path1();");
}

function Paths::GranKar::path1() {
	if(!$Paths::GranKar::pathing) return;
	Movement::initialisePath("Paths::GranKar::restart();");
	Movement::queueCardinalMove("E", 5);
	Movement::queueCardinalMove("NW", 2);
	Movement::queueCardinalMove("E", 1);
	Movement::queueCardinalMove("SE", 1.5);
	Movement::queueCardinalMove("W", 5);
	Movement::queueCardinalMove("N", 1);
	Movement::queueCardinalMove("SW", 2);
	Movement::queueCardinalMove("SE", 4);
	Movement::queueCardinalMove("NE", 3);
	Movement::queueCardinalMove("E", 4);
	Movement::queueCardinalMove("NE", 3);
	Movement::queueCardinalMove("E", 9);
	Movement::queueCardinalMove("W", 1.5);
	Movement::queueCardinalMove("NW", 2);
	Movement::queueCardinalMove("W", 2);
	Movement::queueCardinalMove("N", 5);
	Movement::queueCardinalMove("S", 3);
	Movement::queueCardinalMove("W", 0.25);
	Movement::queueCardinalMove("S", 1.5);
	Movement::queueCardinalMove("W", 0.8);
	Movement::queueCardinalMove("N", 3);
	Movement::queueCardinalMove("E", 0.8);

	Movement::queueCardinalMove("S", 2);
	Movement::queueCardinalMove("W", 0.25);
	Movement::queueCardinalMove("S", 0.25);
	Movement::queueCardinalMove("W", 2);
	Movement::queueCardinalMove("E", 0.3);
	Movement::queueLookUpDown(105);
	Movement::queueCallback("Paths::GranKar::path2();");

	Movement::travelPath();
}

function Paths::GranKar::path2() {
	if(!$Paths::GranKar::pathing) return;
	Movement::smartTurnAngle(-0.79, "Paths::GranKar::path3();");
}

function Paths::GranKar::path3() {
	if(!$Paths::GranKar::pathing) return;
	Equipment::equip("Long Sword");
	Actions::startAutoAttack();
	AutoRemort::castBestSpell();
}