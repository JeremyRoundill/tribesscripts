// Template.cs

// Instructions:
// Find and replace "Template" with the name of the path you want (no spaces, try and make it match the filename)

$Paths::Template::pathing = false;

$Paths::Template::menuKey = ""; // INSTRUCTIONS: Shortcut key for menu. Example "t"
$Paths::Template::menuDescription = ""; // INSTRUCTIONS: Description for menu, keep it short. Example "Auto walk to Ethren Bank"
$Paths::Template::menuFunction = "Paths::Template::setup();"; // INSTRUCTIONS: Function to call to walk path. Probably leave as is

function Paths::Template::stop() {
	$Paths::Template::pathing = false;
	Movement::stop();
	// INSTRUCTIONS: Add a Schedule::Cancel here for every Schedule::Add you have in the rest of this script
	//   For example:
	//   Schedule::Cancel("Movement::splitPath(\"Paths::Template::path1\");");
	//   is what should be added if the following exists in the rest of the script
	//   Schedule::Add("Movement::splitPath(\"Paths::Template::path1\");", 5);
}

function Paths::Template::restart(){
	if(!$Paths::Template::pathing) return;
	Paths::Template::stop();
	Paths::Template::setup();
}

function Paths::Template::setup() {
	$Paths::Template::pathing = true;
	say(2, "#cast transport SOMEPLACE"); // INSTRUCTIONS: make this a transport to where you want to start from
	Movement::initialisePath("Paths::Template::restart();");
	Movement::smartTurnCardinal(NE, "Paths::Template::start();"); // INSTRUCTIONS: change the cardinal direction here to the direction you want to face while pathing
}

function Paths::Template::start() {
	if(!$Paths::Template::pathing) return;
	Schedule::Add("Movement::splitPath(\"Paths::Template::path1\");", 5); // This is a delay so pathing doesn't begin before transport has been cast
}

function Paths::Template::path1(%direction, %distance) {
	if(!$Paths::Template::pathing) return;

	if(%distance == -1 && %direction == -1) {
		Paths::Template::stop();
		Schedule::Add("Paths::JatenBank::NoPack::setup(\"Paths::Template::setup();\");", 20);
		return;
	}

	// INSTRUCTIONS: There are always 4 different places you can transport into.
	//   Work out the pack distance at each of these, and create a different path for
	//   each of these places. You want to end up in a consistant place from each
	//   different transport location, so try to walk into a corner or something
	
	if(%distance == 4238 || %distance == 4239) { // N corner
		Movement::queueCardinalMove(SE, 0.4);
		Movement::queueCardinalMove(SW, 0.4);
	} else if(%distance == 4242 ||%distance == 4243) { // E corner
		Movement::queueCardinalMove(NW, 0.4);
		Movement::queueCardinalMove(SW, 0.4);
	} else if(%distance == 4249 || %distance == 4250) { // S corner
		Movement::queueCardinalMove(NW, 0.4);
		Movement::queueCardinalMove(NE, 0.4);
	} else if(%distance == 4245 || %distance == 4246) { // W corner
		Movement::queueCardinalMove(SE, 0.4);
		Movement::queueCardinalMove(NE, 0.4);
	} else {
		Paths::Template::restart();
		return;
	}
	
	// INSTRUCTIONS: After you've gotten to a consistent location, perform the remainder
	//    of the path
	
	Movement::queueDelay(1); // This is a wait
	Movement::queueCardinalMove(SW, 3); // This moves in a direction
	Movement::queueSanityCheck("North", 3042); // This checks that the pack is a certain distance away in a certain direction
	Movement::queueLookUpDown(105); // This looks at 105 degrees from straight up
	Movement::queueCallback("Paths::Template::path2();"); // This is the function which is called when the path is done
	Movement::travelPath(); // Start going along the path
}

function Paths::Template::path2() { // This is what's called at the end of the path
	if(!$Paths::Template::pathing) return;
	Actions::startAutoAttack();
	AutoRemort::castBestSpell();
}
