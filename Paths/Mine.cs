// Mine.cs


$Paths::Mine::pathing = false;

$Paths::Mine::menuKey = "m";
$Paths::Mine::menuDescription = "Auto walk to Mines";
$Paths::Mine::menuFunction = "Paths::Mine::setup();";

function Paths::Mine::stop() {
	$Paths::Mine::pathing = false;
	Movement::stop();
	Schedule::Cancel("Paths::Mine::path();");
	Schedule::Cancel("Paths::JatenBank::NoPack::setup(\"Paths::Mine::setup();\");");	
}

function Paths::Mine::restart() {
	if(!$Paths::Mine::pathing) return;
	Paths::Mine::stop();
	Paths::Mine::setup();
}

function Paths::Mine::setup() {
	$Paths::Mine::pathing = true;
	say(2, "#cast transport mine");
	Equipment::equip("Wind Walkers");
	Movement::initialisePath("Paths::Mine::restart();");
	Movement::smartTurnCardinal(S, "Paths::mine::start();");
}

function Paths::Mine::start() {
	if(!$Paths::Mine::pathing) return;
	Schedule::Add("Movement::splitPath(\"Paths::Mine::path\");", 5);	
}

function Paths::Mine::path(%distance, %direction) {
	if(!$Paths::Mine::pathing) return;

	if(%distance == -1 && %direction == -1) {
		Paths::Mine::stop();
		Schedule::Add("Paths::JatenBank::NoPack::setup(\"Paths::Mine::setup();\");", 20);
		return;
	}

	Movement::queueCardinalMove(SE, 4); // Run into SE corner of mining hut
	Movement::queueCardinalMove(N, 2); // Run into fireplace
	Movement::queueCardinalMove(S, 0.2); // Get free of fireplace
	Movement::queueCardinalMove(W, 3.3); // Run out door
	Movement::queueCardinalMove(SE, 20); // Run into corner of mine entrance
	Movement::queueCardinalMove(N, 0.5); // Get free of corner
	Movement::queueCardinalMove(E, 1); // Run infront of door
	Movement::queueJump(0.4); // Schedule jump over lip
	Movement::queueCardinalMove(S, 12); // Run into mine door, and into first chamber on left
	Movement::queueCardinalMove(SE, 2); // Position in first chamber
	Movement::queueCardinalMove(SW, 2); // Index against corner
	Movement::queueSanityCheck("North East", 3175);
	Movement::queueCardinalMove(NW, 0.25);
	Movement::queueCardinalMove(E, 8);
	Movement::queueCardinalMove(SE, 2);
	Movement::queueCardinalMove(S, 1.5);
	Movement::queueCardinalMove(SW, 1.5);
	Movement::queueCardinalMove(NW, 3);
	Movement::queueSanityCheck("North East", 3179);
	Movement::queueCardinalMove(E, 0.25);
	Movement::queueCardinalMove(N, 1);
	Movement::queueJet(3, 1);
	Movement::queueCardinalMove(W, 8);
	Movement::queueCardinalMove(SW, 2);
	Movement::queueSanityCheck("North East", 3202);
	Movement::queueCardinalMove(N, 4);
	Movement::queueCardinalMove(SE, 0.25);
	Movement::queueCardinalMove(E, 10);
	Movement::queueJet(1, 5);
	Movement::queueCardinalMove(E, 15);

	Movement::queueJet(0.2, 8);
	Movement::queueDelay(7);
	Movement::queueCardinalMove(E, 3);
	Movement::queueCardinalMove(W, 0.3);
	Movement::queueJet(0.1,6);
	Movement::queueDelay(2);
	Movement::queueCardinalMove(NE, 4);

	Movement::queueLookUpDown(105);
	Movement::queueCallback("Paths::Mine::path2();");
	Movement::travelPath();
}

function Paths::Mine::path2() {
	if(!$Paths::Mine::pathing) return;
	%cardinalAngle = Movement::cardinalToRad(SW);
	%offsetAngle = deg2rad(5);
	%angleToTurn = %cardinalAngle - %offsetAngle;

	Movement::smartTurnAngle(%angleToTurn, "Paths::Mine::path3();");
}

function Paths::Mine::path3() {
	if(!$Paths::Mine::pathing) return;

	if(getItemCount("Thorr's Hammer")) {
		Equipment::equip("Thorr's Hammer");
	} else if (getItemCount("Pick Axe")) {
		Equipment::equip("Pick Axe");
	} else {
		buy("Pick Axe");
		schedule("Equipment::equip(\"Pick Axe\");", 1);
	}

	Mining::startMine(true);
	Mining::resetMine();
}