// Uber.cs

$Paths::Uber::pathing = false;

$Paths::Uber::menuKey = "u";
$Paths::Uber::menuDescription = "Auto walk to Uber";
$Paths::Uber::menuFunction = "Paths::Uber::setup();";

function Paths::Uber::stop() {
	$Paths::Uber::pathing = false;
	Movement::stop();
	Schedule::Cancel("Movement::splitPath(\"Paths::Uber::path1\");");
	Schedule::Cancel("Paths::JatenBank::NoPack::setup(\"Paths::Uber::setup();\");");
}

function Paths::Uber::restart(){
	if(!$Paths::Uber::pathing) return;
	Paths::Uber::stop();
	Paths::Uber::setup();
}

function Paths::Uber::setup() {
	$Paths::Uber::pathing = true;
	say(2, "#cast transport uber");
	Movement::initialisePath("Paths::Uber::restart();");
	//Movement::initialisePath();
	$Movement::CardinalHeading = NE;
	Movement::smartTurnCardinal(NE, "Paths::Uber::start();");
}

function Paths::Uber::start() {
	if(!$Paths::Uber::pathing) return;
	Schedule::Add("Movement::splitPath(\"Paths::Uber::path1\");", 5);
}

function Paths::Uber::path1(%direction, %distance) {
	if(!$Paths::Uber::pathing) return;

	if(%distance == -1 && %direction == -1) {
		Paths::Uber::stop();
		Schedule::Add("Paths::JatenBank::NoPack::setup(\"Paths::Uber::setup();\");", 20);
		return;
	}

	if(%distance == 4238 || %distance == 4239) { // N corner
		Movement::queueCardinalMove(SE, 0.4);
		Movement::queueCardinalMove(SW, 0.4);
	} else if(%distance == 4242 ||%distance == 4243) { // E corner
		Movement::queueCardinalMove(NW, 0.4);
		Movement::queueCardinalMove(SW, 0.4);
	} else if(%distance == 4249 || %distance == 4250) { // S corner
		Movement::queueCardinalMove(NW, 0.4);
		Movement::queueCardinalMove(NE, 0.4);
	} else if(%distance == 4245 || %distance == 4246) { // W corner
		Movement::queueCardinalMove(SE, 0.4);
		Movement::queueCardinalMove(NE, 0.4);
	} else {
		Paths::Uber::restart();
		return;
	}
	Movement::queueDelay(1);
	Movement::queueCardinalMove(SW, 3);
	Movement::queueSanityCheck("North", 3042);
	Movement::queueCardinalMove(NE, 0.3);
	Movement::queueCardinalMove(N, 4);
	Movement::queueCardinalMove(SW, 0.3);
	Movement::queueCardinalMove(NW, 2);
	Movement::queueSanityCheck("North", 3015);
	Movement::queueCardinalMove(S, 0.2);
	Movement::queueCardinalMove(W, 10);
	Movement::queueLookUpDown(105);
	Movement::queueCallback("Paths::Uber::path2();");
	Movement::travelPath();
}

function Paths::Uber::path2() {
	if(!$Paths::Uber::pathing) return;
	Actions::startAutoAttack();
	AutoRemort::castBestSpell();
}
