// JatenBank.cs

%i = -1;

function Paths::JatenBank::stop() {
	Movement::stop();
	schedule::Cancel("Paths::JatenBank::NoPack::path();");
	Schedule::Cancel("Paths::JatenBank::NoPack::path3();");
	Schedule::Cancel("Equipment::startDropTrash();");
	Schedule::Cancel($Paths::JatenBank::callback);
	Schedule::Cancel("Movement::splitPath(\"Paths::JatenBank::CheckPackCallback\");");

	$Paths::JatenBank::pathing = false;
	$Paths::JatenBank::callback = "";
}

function Paths::JatenBank::MerchantResponse() {
	$Paths::JatenBank::MerchantResponse = true;
}

function Paths::JatenBank::NoPack::restart() {
	if(!$Paths::JatenBank::pathing) return;
	Paths::JatenBank::stop();
	Paths::JatenBank::NoPack::setup();	
}

function Paths::JatenBank::NoPack::setup(%callback) {
	$Paths::JatenBank::pathing = true;
	$Paths::JatenBank::callback = %callback;
	$Paths::JatenBank::droppingTrash = $Equipment::droppingTrash;
	if($Paths::JatenBank::droppingTrash) {
		Equipment::stopDropTrash();
	}
	say(2, "#cast transport jaten");
	Movement::initialisePath();
	Movement::smartTurnCardinal(W, "Paths::JatenBank::NoPack::start();");
}

function Paths::JatenBank::NoPack::start() {
	if(!$Paths::JatenBank::pathing) return;
	schedule::Add("Paths::JatenBank::NoPack::path();", 5);
}

function Paths::JatenBank::NoPack::path() {
	if(!$Paths::JatenBank::pathing) return;

	Movement::queueCardinalMove(NW, 12); // This seems long, but the merchant can speak a long way through walls
	Movement::queueCallback("Paths::JatenBank::NoPack::path2();");

	Movement::travelPath();
}

function Paths::JatenBank::NoPack::path2() {
	$Paths::JatenBank::MerchantResponse = false;
	ChatEvents::register("merchant tells you, \"Take a look at what I have.\"", "Paths::JatenBank::MerchantResponse");
	Actions::doAction("say hi");
	Actions::scheduleAction("say buy", 2);
	schedule("buy(\"Basic Arrow\");", 3);
	schedule("buy(\"Basic Arrow\");", 4);
	Schedule::Add("Paths::JatenBank::NoPack::path3();", 5);
}

function Paths::JatenBank::NoPack::path3() {
	if(!$Paths::JatenBank::MerchantResponse) {
		echo("We didn't find the merchant :(");
		Paths::JatenBank::NoPack::restart();
		return;
	}
	remoteEval(2048, ToggleInventoryMode);
	Movement::initialisePath();
	Movement::queueCardinalMove(S, 3.5);
	Movement::queueCardinalMove(SW, 1);
	Movement::queueCardinalMove(S, 1.5);
	Movement::queueJump(0.1);
	Movement::queueCardinalMove(S, 0.6);
	Movement::queueJump(0.1);
	Movement::queueJump(0.4);
	Movement::queueJump(0.7);
	Movement::queueCardinalMove(SW, 2);
	Movement::queueCallback("Paths::JatenBank::NoPack::path4();");
	Movement::travelPath();
}


function Paths::JatenBank::NoPack::path4() {
	Movement::smartTurnCardinal("SW", "Paths::JatenBank::NoPack::path5();");
	sell("Basic Arrow");
}

function Paths::JatenBank::NoPack::path5() {
	Actions::doAction("createpack");
	Movement::cardinalMove("NE", 1);

	Actions::scheduleAction("say storage", 1);
	Actions::scheduleAction("say hi", 2);
	Actions::scheduleAction("say storage", 3);

	if($Paths::JatenBank::droppingTrash) {
		schedule("Equipment::startDropTrash();", 1);
	}

	if($Paths::JatenBank::callback != "" && $Paths::JatenBank::callback != -1) {
		schedule($Paths::JatenBank::callback, 4);
	}
}

function Paths::JatenBank::OpenBank(%callback) {
	Actions::castSpell("transport jaten");
	$Paths::JatenBank::pathing = true;
	$Paths::JatenBank::callback = %callback;
	Movement::initialisePath();
	Schedule::Add("Movement::splitPath(\"Paths::JatenBank::OpenBankCallback\");", 5);
}

function Paths::JatenBank::OpenBankCallback(%direction, %distance) {
	// No pack placed, so let's place one!
	if(%direction == -1 && %distance == -1) {
		Paths::JatenBank::NoPack::path();
		return;
	}

	// Sane responses:
	// W 52
	// W 55
	// W 61 - looks like the one which doesn't work for running straight in
	// W 64
	if(
		%direction == "West" &&
		(
			%distance == 52 ||
			%distance == 55 || 
			%distance == 64
		)
	) {
		Movement::queueCardinalMove(NW, 8);
		Movement::queueCallback("Paths::JatenBank::NoPack::path2();");
		Movement::travelPath();
		// We're sane and the pack exists!
	} else if (%direction == "West" && %distance == 61) {
		Movement::queueCardinalMove(SW, 3);
		Movement::queueCardinalMove(NW, 5);
		Movement::queueCallback("Paths::JatenBank::NoPack::path2();");
		Movement::travelPath();
	}


	// An insane response requires resetting the pack.
}