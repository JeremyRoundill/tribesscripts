// Paths.cs

exec("Jeremy\\Paths\\Crypt.cs");
exec("Jeremy\\Paths\\GranKar.cs");
exec("Jeremy\\Paths\\JatenBank.cs");
exec("Jeremy\\Paths\\Mine.cs");
exec("Jeremy\\Paths\\Minotaur.cs");
exec("Jeremy\\Paths\\Uber.cs");

// Register paths here

%i = -1;
$Paths[%i++] = "Crypt";
$Paths[%i++] = "GranKar";
$Paths[%i++] = "JatenBank";
$Paths[%i++] = "Mine";
$Paths[%i++] = "Minotaur";
$Paths[%i++] = "Uber";


function Paths::init() {

	Menu::New(PathMenu, "Available Paths");

	for(%i = 0; $Paths[%i] != ""; %i++) {
		%pathName = $Paths[%i];

		eval("%pathKey = $Paths::" @ %pathName @ "::menuKey;");
		eval("%pathDescription = $Paths::" @ %pathName @ "::menuDescription;");
		eval("%pathFunction = $Paths::" @ %pathName @ "::menuFunction;");

		%pathString = %pathKey @ %pathDescription;

  		Menu::AddChoice(PathMenu, %pathString, %pathFunction);
	}	

    Menu::AddChoice(PathMenu, "sStop pathing.", "Paths::stopAll();");
}

function Paths::stopAll() {
	for(%i = 0; $Paths[%i] != ""; %i++) {
		%pathName = $Paths[%i];
		eval("Paths::" @ %pathName @ "::stop();");
	}
}
