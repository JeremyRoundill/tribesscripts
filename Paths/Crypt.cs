// Crypt.cs

$Paths::Crypt::pathing = false;

$Paths::Crypt::menuKey = "c";
$Paths::Crypt::menuDescription = "Auto walk to Crypt";
$Paths::Crypt::menuFunction = "Paths::Crypt::setup();";

function Paths::Crypt::stop() {
	$Paths::Crypt::pathing = false;
	Movement::stop();
	Schedule::Cancel("Movement::splitPath(\"Paths::Crypt::path1\");");
	Schedule::Cancel("Paths::JatenBank::NoPack::setup(\"Paths::Crypt::setup();\");");
}

function Paths::Crypt::restart() {
	if(!$Paths::Crypt::pathing) return;
	Paths::Crypt::stop();
	Paths::Crypt::setup();
}

function Paths::Crypt::setup() {
	$Paths::Crypt::pathing = true;
	say(2, "#cast transport crypt");
	Movement::initialisePath("Paths::Crypt::restart();");
	Movement::smartTurnCardinal(W, "Paths::Crypt::start();");
}

function Paths::Crypt::start() {
	if(!$Paths::Crypt::pathing) return;
	Schedule::Add("Movement::splitPath(\"Paths::Crypt::path1\");", 5);
}

function Paths::Crypt::path1(%direction, %distance) {
	if(!$Paths::Crypt::pathing) return;

	if(%distance == -1 && %direction == -1) {
		Paths::Crypt::stop();
		Schedule::Add("Paths::JatenBank::NoPack::setup(\"Paths::Crypt::setup();\");", 20);
		return;
	}

	// 1085
	// 1086
	if(%distance == 1078) { // works
		Movement::queueCardinalMove(W, 35.5);
		Movement::queueCardinalMove(S, 2);
		Movement::queueCardinalMove(SW, 1.3);
		Movement::queueCardinalMove(S, 3);
	} else if(%distance == 1079) { // falls short from the looks
		Movement::queueCardinalMove(W, 32.2);
		Movement::queueCardinalMove(SW, 5);
		Movement::queueCardinalMove(S, 1);
		Movement::queueCardinalMove(NE, 0.1);
		Movement::queueCardinalMove(S, 1);
	} else if(%distance == 1085) { // works
		Movement::queueCardinalMove(W, 35.5);
		Movement::queueCardinalMove(SW, 1.2);
		Movement::queueCardinalMove(S, 2);
		Movement::queueCardinalMove(NE, 0.1);
		Movement::queueCardinalMove(S, 1);
	} else if(%distance == 1086) { // works
		Movement::queueCardinalMove(W, 34);
		Movement::queueCardinalMove(SW, 2);
		Movement::queueCardinalMove(SE, 1);
		Movement::queueCardinalMove(S, 2);
	} else {
		Paths::Crypt::restart();
		return;
	}

	Movement::queueCardinalMove(SW, 1);
	Movement::queueSanityCheck("North East", 1176);
	Movement::queueCardinalMove(NE, 0.2);
	Movement::queueCardinalMove(N, 7); // we need a sanity check here too. The first one sometimes passes even if we don't get in the crypt

	Movement::queueCardinalMove(SW, 1);
	Movement::queueCardinalMove(S, 8);
	Movement::queueCardinalMove(E, 0.8);
	Movement::queueLookUpDown(105);
	Movement::queueCallback("Paths::Crypt::path2();");
	Movement::travelPath();

}

function Paths::Crypt::path2() {
	if(!$Paths::Crypt::pathing) return;
	Movement::smartTurnCardinal(NE, "Paths::Crypt::path3();");
}

function Paths::Crypt::path3() {
	if(!$Paths::Crypt::pathing) return;
	Actions::startAutoAttack();
	AutoRemort::castBestSpell();
}
