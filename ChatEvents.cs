// ChatEvents.cs
// init

function ChatEvents::init() {
    Dict::New(chatevents);
    Event::Attach(eventClientMessage, messageReceived);
    echo("Init chat");
}


function ChatEvents::Reset() {
    Dict::Reset(chatevents);
}

function ChatEvents::Register(%match, %callback) {
    return Dict::SetByKey(chatevents, %match, %callback);
}

function ChatEvents::Deregister(%match) {
    return Dict::SetByKey(chatevents, %match);
}

// Wait on message
function messageReceived(%client, %msg) {
	for(%i = 0; %i < Dict::Count(chatevents); %i++) {
		%pattern = Dict::Key(chatevents, %i);
        deleteVariables("$msgMatches*");
		if(Message::GetMatchesFromPattern(%msg, %pattern, msgMatches)) {
			// echo(Dict::GetByKey(chatevents, %pattern) @ "(\"" @ Message::escape($msgMatches[0]) @ "\");");

            %argString = "\"" @ Message::escape($msgMatches[0]) @ "\"";
            for(%j = 1; $msgMatches[%j] != ""; %j++) {
                %argString = %argString @ ", \"" @ Message::escape($msgMatches[%j]) @ "\"";
            }

            echo(Dict::GetByKey(chatevents, %pattern) @ "(" @ %argString @ ");");
			eval(Dict::GetByKey(chatevents, %pattern) @ "(" @ %argString @ ");");
		}
	}
}

function Message::Length(%message) {
    for (%i = 0; String::GetSubStr(%message, %i, 1) != ""; %i++) {}
    return %i;
}

function Message::escape(%message) {
    return String::replace(%message, "\"", "\\\"");
}

function Message::GetMatchesFromPattern(%message, %pattern, %matchArray) {
    %matched = true;
    %matchIndex = -1;
    %messageLength = 10000;
    
    eval("$" @ %matchArray @ " = -1;");

    if(String::FindSubStr(%pattern, "*") == 0) {
        %pattern = String::GetSubStr(%pattern, 1, %messageLength);

        %pos = String::FindSubStr(%pattern, "*");
        if(%pos < 0) {
            %matchPos = String::FindSubStr(%message, %pattern);
            if(%matchPos == -1) {
                eval("$" @ %matchArray @ " = -1;");
                return false;
            }

            %toMatch = %pattern;
            %matchPos = String::FindSubStr(%message, %toMatch);
            %match = Message::escape(String::GetSubStr(%message, 0, %matchPos));

            eval("$" @ %matchArray @ "[" @ %matchIndex++ @ "] = \"" @ %match @ "\";");
            return true;
        }

        %toMatch = String::GetSubStr(%pattern, 0, %pos);
        %matchPos = String::FindSubStr(%message, %toMatch);
        %match = Message::escape(String::GetSubStr(%message, 0, %matchPos));
        %length = Message::Length(%toMatch);

        eval("$" @ %matchArray @ "[" @ %matchIndex++ @ "] = \"" @ %match @ "\";");
    }
    
    while(%matched) {
        %matched = false;
        
        %pos = String::FindSubStr(%pattern, "*");
        if(%pos < 0) {
            %matchPos = String::FindSubStr(%message, %pattern);
            if(%matchPos == 0) {
                return true;
            }
            eval("$" @ %matchArray @ " = -1;");
            return false;
        }

        %toMatch = String::GetSubStr(%pattern, 0, %pos);
        %pattern = String::GetSubStr(%pattern, %pos + 1, %messageLength);
        
        if(Message::Length(%pattern) <= 0) {
            %match = %message;
            break;
        }
        
        %matchPos = String::FindSubStr(%message, %toMatch);
        %length = Message::Length(%toMatch);
        %message = String::GetSubStr(%message, %matchPos + %length, %messageLength);
        
        if(%matchPos >= 0) {
            %nextPos = String::FindSubStr(%pattern, "*");
            if(%nextPos >= 0) {
                %nextMatch = String::GetSubStr(%pattern, 0, %nextPos);
            } else {
                %nextMatch = %pattern;
            }
            
            %nextMatchPos = String::FindSubStr(%message, %nextMatch);
            %match = Message::escape(String::GetSubStr(%message, 0, %nextMatchPos));
            
            if(%match == "") {
	            deleteVariables("$" @ %matchArray @ "*");
                return false;
            }
            
            %message = String::GetSubStr(%message, %nextMatchPos, %messageLength);
            
            eval("$" @ %matchArray @ "[" @ %matchIndex++ @ "] = \"" @ %match @ "\";");
            // echo(%match);
            %matched = true;
        } else {
	            deleteVariables("$" @ %matchArray @ "*");
            return false;
        }
        
        if(%matchIndex > 15) break;
    }
  
    return false;
}

