// Movement.cs

$Movement::CardinalHeading = -1;
$Movement::CardinalOffset = 0;
$Movement::coarseSearch = false;
$Movement::fineSearch = false;


$Movement::CardinalDirection["N"] = 0;
$Movement::CardinalDirection["NNE"] = 360/16;
$Movement::CardinalDirection["NE"] = 2 * 360/16;
$Movement::CardinalDirection["ENE"] = 3 * 360/16;
$Movement::CardinalDirection["E"] = 4 * 360/16;
$Movement::CardinalDirection["ESE"] = 5 * 360/16;
$Movement::CardinalDirection["SE"] = 6 * 360/16;
$Movement::CardinalDirection["SSE"] = 7 * 360/16;
$Movement::CardinalDirection["S"] = 8 * 360/16;
$Movement::CardinalDirection["SSW"] = 9 * 360/16;
$Movement::CardinalDirection["SW"] = 10 * 360/16;
$Movement::CardinalDirection["WSW"] = 11 * 360/16;
$Movement::CardinalDirection["W"] = 12 * 360/16;
$Movement::CardinalDirection["WNW"] = 13 * 360/16;
$Movement::CardinalDirection["NW"] = 14 * 360/16;
$Movement::CardinalDirection["NNW"] = 15 * 360/16;


$Movement::PathSteps = -1;
$Movement::PathIndex = -1;
$Movement::PathIndexCurrent = -1;
$Movement::Pathing = false;

$Movement::TrackPackName = "";

$Movement::PackDirection = -1;
$Movement::PackDistance = -1;

function Movement::init() {
	if($Movement::TrackPackName == "") {
		$Movement::TrackPackName = Client::getName(getManagerId());
	}

	%trackString = $Movement::TrackPackName @ "'s nearest backpack is * of here, * meters away.";

	ChatEvents::register(%trackString, Movement::sanityCallback);
}

function Movement::stop() {
	deleteVariables("$Movement::PathSteps*");
	$Movement::Pathing = false;
	$Movement::PathIndex = 0;
	$Movement::PathIndexCurrent = 0;
	Movement::killTurn();
	
	postAction(2048, IDACTION_MOVEFORWARD, 0);
	postAction(2048, IDACTION_MOVEBACK, 0);
	postAction(2048, IDACTION_MOVELEFT, 0);
	postAction(2048, IDACTION_MOVERIGHT, 0);
	postAction(2048, IDACTION_LOOKDOWN, 0);
	postAction(2048, IDACTION_LOOKUP, 0);
	postAction(2048, IDACTION_TURNLEFT, 0);
	postAction(2048, IDACTION_TURNRIGHT, 0);
}

function Movement::sanityCallback(%direction, %distance) {
	echo("cb " @ %direction @ " - " @ %distance);
	$Movement::PackDirection = %direction;
	$Movement::PackDistance = %distance;
}

function Movement::scheduleInput(%input, %amount, %delay) {
	schedule("postaction(2048, " @ %input @ ", " @ %amount @ ");", %delay);
}

function Movement::lookUpDown(%angle, %speed) {
	// Takes 96 seconds to look from 0deg (up) to 180deg (down) at speed 0.001

	// This is our minimum speed
	if(%speed < 0.001) %speed = 0.001;

	// Formula:

	// b = 0.001 (base speed - arbitrary number which is pretty slow and therefore more accurate given constant timing errors)
	// d = 96 (time taken to turn from 0deg to 180deg at base speed i.e. 0.001)
	// s = given speed
	// a = given angle

	// t = time to turn

	// t = d * b/s * a/180
	// t = (d*b*a)/(s*180)
	// t = (96*0.001*a)/(s*180)
	// t = 0.096*a/180*s


	%time = (0.097 * %angle) / (180 * %speed);

	postaction(2048, IDACTION_LOOKUP, 5);
	Movement::scheduleInput(IDACTION_LOOKUP, 0, 0.2);
	Movement::scheduleInput(IDACTION_LOOKDOWN, %speed, 0.4);
	Movement::scheduleInput(IDACTION_LOOKDOWN, 0, 0.4 + %time);

	return %time + 0.4;
}

function Movement::cardinalToRad(%direction) {
	%angle = $Movement::CardinalDirection[%direction];

	if(%angle == 0) return 0;

	if(%angle < 180) {
		%angle = -%angle;
	} else {
		%angle = 360 - %angle;
	}

	return(deg2rad(%angle));
}

function Movement::cardinalMove(%direction, %duration) {
	if($Movement::CardinalHeading == -1) {
		echo("Error: I don't know where I'm looking");
		return;
	}

	%currentAngle = $Movement::CardinalDirection[$Movement::CardinalHeading];
	%desiredAngle = $Movement::CardinalDirection[%direction];

	if(%currentAngle > %desiredAngle) %desiredAngle = %desiredAngle + 360;
	%angleToMove = %desiredAngle - %currentAngle;

	if(%angleToMove == 0) {
		postaction(2048, IDACTION_MOVEFORWARD, 1);
	} else if (%angleToMove == 45) {
		postaction(2048, IDACTION_MOVEFORWARD, 1);
		postaction(2048, IDACTION_MOVERIGHT, 1);
	} else if (%angleToMove == 90) {
		postaction(2048, IDACTION_MOVERIGHT, 1);
	} else if (%angleToMove == 135) {
		postaction(2048, IDACTION_MOVEBACK, 1);
		postaction(2048, IDACTION_MOVERIGHT, 1);
	} else if (%angleToMove == 180) {
		postaction(2048, IDACTION_MOVEBACK, 1);
	} else if (%angleToMove == 225) {
		postaction(2048, IDACTION_MOVEBACK, 1);
		postaction(2048, IDACTION_MOVELEFT, 1);
	} else if (%angleToMove == 270) {
		postaction(2048, IDACTION_MOVELEFT, 1);
	} else if (%angleToMove == 315) {
		postaction(2048, IDACTION_MOVEFORWARD, 1);
		postaction(2048, IDACTION_MOVELEFT, 1);
	}


	Movement::scheduleInput(IDACTION_MOVEFORWARD, 0, %duration);
	Movement::scheduleInput(IDACTION_MOVEBACK, 0, %duration);
	Movement::scheduleInput(IDACTION_MOVELEFT, 0, %duration);
	Movement::scheduleInput(IDACTION_MOVERIGHT, 0, %duration);

	return %duration;
}

function Movement::asyncJump(%delay) {
	Movement::scheduleInput(IDACTION_MOVEUP, 1, %delay);
	return 0;
}

function Movement::asyncJet(%delay, %duration) {
	Movement::scheduleInput(IDACTION_MOVEUP, 1, %delay);
	Movement::scheduleInput(IDACTION_JET, 1, %delay);
	Movement::scheduleInput(IDACTION_JET, 0, %delay + %duration);
	return 0;
}

function Movement::delay(%delay) {
	return %delay;
}


function Movement::sanityCheck(%direction, %distance) {
	$Movement::PackDirection = -1;
	$Movement::PackDistance = -1;
	say(2, "#trackpack " @ $Movement::TrackPackName);
	schedule("Movement::sanityCheck2(\"" @ %direction @ "\", " @ %distance @ ");", 2);
	return 3;
}

function Movement::sanityCheck2(%direction, %distance) {
	if($Movement::PackDirection != %direction || $Movement::PackDistance != %distance) {
		$Movement::Pathing = false;
		if($Movement::InsaneCallback != -1 && $Movement::InsaneCallback != "") {
			eval($Movement::InsaneCallback);
		}
		echo("We're insane, I'm afraid");
	} else {
		echo("We're good to go");
	}
}


function Movement::splitPath(%callback) {
	$Movement::PackDirection = -1;
	$Movement::PackDistance = -1;
	say(2, "#trackpack " @ $Movement::TrackPackName);
	schedule("Movement::splitPath2(\"" @ %callback @ "\");", 2);
	return 3;
}

function Movement::splitPath2(%callback) {
	echo(%callback @ "(\"" @ $Movement::PackDirection @ "\", " @ $Movement::PackDistance @ ");");
	eval(%callback @ "(\"" @ $Movement::PackDirection @ "\", " @ $Movement::PackDistance @ ");");
}


function Movement::initialisePath(%insaneCallback) {
	deleteVariables("$Movement::PathSteps*");
	$Movement::Pathing = false;
	$Movement::PathIndex = 0;
	$Movement::PathIndexCurrent = 0;
	$Movement::InsaneCallback = %insaneCallback;

	//Movement::queueResetToCardinal();
	//Movement::queueLookUpDown(92);
}

function Movement::queueResetToCardinal() {
	$Movement::PathSteps[$Movement::PathIndex] = "Movement::resetToCardinal();";
	$Movement::PathIndex++;
}

function Movement::queueCardinalMove(%direction, %duration) {
	$Movement::PathSteps[$Movement::PathIndex] = "Movement::cardinalMove(" @ %direction @ ", " @ %duration @ ");";
	$Movement::PathIndex++;
}

function Movement::queueOffsetCardinal(%angle) {
	$Movement::PathSteps[$Movement::PathIndex] = "Movement::offsetCardinal(" @ %angle @ ");";
	$Movement::PathIndex++;
}

function Movement::queueLookUpDown(%angle) {
	$Movement::PathSteps[$Movement::PathIndex] = "Movement::lookUpDown(" @ %angle @ ", 0.02);";
	$Movement::PathIndex++;
}

function Movement::queueJump(%delay) {
	$Movement::PathSteps[$Movement::PathIndex] = "Movement::asyncJump(" @ %delay @ ");";
	$Movement::PathIndex++;
}

function Movement::queueJet(%delay, %duration) {
	$Movement::PathSteps[$Movement::PathIndex] = "Movement::asyncJet(" @ %delay @ ", " @ %duration @ ");";
	$Movement::PathIndex++;
}

function Movement::queueDelay(%delay) {
	$Movement::PathSteps[$Movement::PathIndex] = "Movement::delay(" @ %delay @ ");";
	$Movement::PathIndex++;
}

function Movement::queueSanityCheck(%direction, %distance) {
	$Movement::PathSteps[$Movement::PathIndex] = "Movement::sanityCheck(\"" @ %direction @ "\", " @ %distance @ ");";
	$Movement::PathIndex++;
}

function Movement::queueCallback(%cb) {
	$Movement::PathSteps[$Movement::PathIndex] = %cb;
	$Movement::PathIndex++;
}

function Movement::travelPath() {
	$Movement::Pathing = true;
	Movement::travelStep();
}

function Movement::travelStep() {
	%step = $Movement::PathSteps[$Movement::PathIndexCurrent];
	if(%step == -1 || %step == "" || !$Movement::Pathing) {
		$Movement::Pathing = false;
		return;
	}

	$Movement::PathIndexCurrent++;
	%time = eval(%step);
	echo(%step);
	Schedule::Add("Movement::travelStep();", %time + 0.1);
}


$Movement::smartTurnCb = -1;
function Movement::killTurn() {
	$Movement::smartTurnCb = -1;
	$Movement::turning = false;
}

function Movement::smartTurn(%direction, %accuracy) {
	if(!$Movement::turning) {
		postAction(2048, IDACTION_TURNLEFT, 0);
		postAction(2048, IDACTION_TURNRIGHT, 0);
		return;
	}

	%rot = getZRotation();

	%difference = %direction - %rot;

	if(%difference > $pi) {
		%difference = 2*$pi - %difference;
	}

	if(%difference < -$pi) {
		%difference = -2*$pi + %difference;
	}

	%absDifference = %difference;
	if(%absDifference < 0) %absDifference = -%absDifference;


	if(%absDifference > %accuracy) {
		%speed = %accuracy/10;
	} 

	if(%absDifference > %accuracy * 20) {
		%speed = %accuracy;
	} 

	if(%absDifference > %accuracy * 100) {
		%speed = %accuracy * 20;
	} 


	// echo("speed: " @ %speed @ " rotation: " @ %rot);

	if(%absDifference <= %accuracy) {
		postAction(2048, IDACTION_TURNLEFT, 0);
		postAction(2048, IDACTION_TURNRIGHT, 0);
		eval($Movement::smartTurnCb);
		return;
	}

	%turn = Movement::calculateTurnDirection(%rot, %direction);

	if(%turn > 0) {
		postAction(2048, IDACTION_TURNLEFT, 0);
		postAction(2048, IDACTION_TURNRIGHT, %speed);
	} else {
		postAction(2048, IDACTION_TURNLEFT, %speed);
		postAction(2048, IDACTION_TURNRIGHT, 0);
	}
	schedule("Movement::smartTurn(" @ %direction @ ", " @ %accuracy @ ");", 0.1);
}

function Movement::smartTurnAngle(%angle, %cb) {
	// Stops current turning if in progress and schedules to start a 
	// new turn
	$Movement::turning = false;
	$Movement::smartTurnCb = %cb;
	schedule("Movement::startSmartTurn(" @ %angle @ ");", 0.3);
}

function Movement::startSmartTurn(%angle) {
	$Movement::turning = true;
	Movement::smartTurn(%angle, 0.001);	
}

function Movement::smartTurnCardinal(%cardinal, %cb) {
	$Movement::CardinalHeading = %cardinal;
	Movement::smartTurnAngle(Movement::cardinalToRad(%cardinal), %cb);
}

// 1 = right
// -1 = left
// 0 = these angles are the same, you dingus
function Movement::calculateTurnDirection(%angle1, %angle2) {
	if(%angle1 == %angle2) return 0;

	if(%angle1 > %angle2) {
		if((%angle1 - %angle2) < $pi) return 1;
		return -1;
	}
	if((%angle2 - %angle1) < $pi) return -1;
	return 1;
}
