exec("Jeremy\\Dict.cs");
exec("Jeremy\\Message.cs");
exec("Jeremy\\Actions.cs");
exec("Jeremy\\Bank.cs");
exec("Jeremy\\ChatEvents.cs");
exec("Jeremy\\Lamp.cs");
exec("Jeremy\\Mining.cs");
exec("Jeremy\\Mug.cs");
exec("Jeremy\\SP.cs");
exec("Jeremy\\TvT.cs");
exec("Jeremy\\Equipment.cs");
exec("Jeremy\\Movement.cs");
exec("Jeremy\\AutoRemort.cs");

exec("Jeremy\\Paths\\Paths.cs");


event::Attach(eventConnected, initScripts);

function initScripts() {
	// Init stuff
	ChatEvents::init();
	Lamp::init();
	AutoRemort::init();
	Movement::init();
	Paths::init();
}

$buyParchments = true;
function buyParchments() {
	if(!$buyParchments) return;
	if(getItemCount("Parchment") >= 100) {
		say(0, "100");
		schedule("sell(\"Parchment\");", 1);
		schedule("buyParchments();", 2);
		return;
	}

	say(1, "#say hi");
	schedule("say(1, \"#say yes\");", 1);
	schedule("buyParchments();", 2);
}

$buyDust = true;
function buyDust() {
	if(!$buyDust) return;
	if(getItemCount("Parchment") == 0) {
		say(0, "100");
		schedule("buy(\"Parchment\");", 1);
		schedule("buyDust();", 2);
		return;
	}
	
	if(getItemCount("Magic Dust") >= 100) {
		say(0, "100");
		schedule("sell(\"Magic Dust\");", 1);
		schedule("buyDust();", 2);
		return;
	}

	say(1, "#say hi");
	schedule("say(1, \"#say yes\");", 1);
	schedule("buyDust();", 2);
}


$sellDust = true;
function sellDust() {
	if(!$sellDust) return;	
	if(getItemCount("Magic Dust") == 0) {
		say(0, "100");
		schedule("buy(\"Magic Dust\");", 1);
		schedule("sellDust();", 2);
		return;
	}

	say(1, "#say hi");
	schedule("say(1, \"#say yes\");", 1);
	schedule("sellDust();", 2);
}


// We do this because all the other scripts do. Don't ask me why.
editActionMap("playmap.sae");

// Utility / heals
bindCommand(keyboard0, make, "-", TO, "remoteeval(2048, say(0, \"#meditate\"));");
bindCommand(keyboard0, make, "+", TO, "remoteeval(2048, say(0, \"#wake\"));");
bindCommand(keyboard0, make, "h", TO, "remoteeval(2048, say(0, \"#cast heal\"));");

// Auto attack
bindCommand(keyboard0, make, "l", TO, "postaction(2048,IDACTION_FIRE1,-0);");

// Move forward
bindCommand(keyboard0, make, ";", TO, "postaction(2048,IDACTION_MOVEFORWARD,1);");


// Bind autocast
bindCommand(keyboard0, make, shift, "1", TO, "Actions::startMediCast(beam);");
bindCommand(keyboard0, make, shift, "2", TO, "Actions::startMediCast(dimensionrift);");
bindCommand(keyboard0, make, shift, "3", TO, "Actions::startMediCast(melt);");
bindCommand(keyboard0, make, shift, "4", TO, "Actions::startMediCast(powercloud);");
bindCommand(keyboard0, make, shift, "5", TO, "Actions::startMediCast(cloud);");
bindCommand(keyboard0, make, shift, "6", TO, "Actions::startMediCast(firebomb);");
bindCommand(keyboard0, make, shift, "7", TO, "Actions::startMediCast(fireball);");
bindCommand(keyboard0, make, shift, "0", TO, "Actions::stopMediCast();");


bindCommand(keyboard0, make, shift, "h", TO, "Actions::startMediCast(heal);");

// Bind setup commands

bindCommand(keyboard0, make, "home", TO, "SP::spend();");
bindCommand(keyboard0, make, "end", TO, "Equipment::levelOneEquip();");


// Equip level 1 armour

function Equipment::levelOneEquip() {
	%i = 0;
	schedule("useItem(17);", %i++);
	schedule("useItem(25);", %i++);
	schedule("useItem(29);", %i++);
	schedule("useItem(35);", %i++);
}
// 17, 18 = Titanium Braced Boots
// 19, 20 = Boots of Gliding
// 25, 26 = Steel Helmet
// 29, 30 = Milled-Steel Bracers
// 35, 36 = Fingerless Gloves


// Print message


$gems[0] = "Small rock";
$gems[1] = "Quartz";
$gems[2] = "Granite";
$gems[3] = "Opal";
$gems[4] = "Jade";
$gems[5] = "Turquoise";
$gems[6] = "Ruby";
$gems[7] = "Topaz";
$gems[8] = "Sapphire";
$gems[9] = "Gold";
$gems[10] = "Emerald";
$gems[11] = "Diamond";
$gems[12] = "Iron";
$gems[13] = "Silver";
$gems[14] = "Titanium";




$gemIndex = 1;

function initBuyGems(%gemIndex) {
	$gemIndex = %gemIndex;
	$gemQuantity = -1;
	$continueBuyingGems = true;
	schedule("remoteeval(2048, say(0, \"hi\"));", 0);
	schedule("remoteeval(2048, say(0, \"deposit\"));", 1);
	schedule("remoteeval(2048, say(0, \"all\"));", 2);
	schedule("remoteeval(2048, say(0, \"hi\"));", 3);
	schedule("remoteeval(2048, say(0, \"storage\"));", 4);
	schedule("buyGems();", 5);
}

function checkGems() {
	$currQuantity = getItemCount($gems[$gemIndex]);
	if($currQuantity == $gemQuantity) {
		$continueBuyingGems = false;
	}
	
	
	// if(DeusRPG::FetchData("Weight") > DeusRPG::FetchData("MaxWeight")) {
	// 	$continueBuyingGems = false;
	// }
	
	$gemQuantity = $currQuantity;
}
	
function buyGems() {
	if(!$continueBuyingGems) {
		echo("Done! " @ $gemQuantity);
		schedule("remoteeval(2048, say(0, \"hi\"));", 0);
		schedule("remoteeval(2048, say(0, \"deposit\"));", 1);
		schedule("remoteeval(2048, say(0, \"all\"));", 2);
		return;
	}
	remoteeval(2048, say(0, "100"));

	schedule("buy(\"" @ $gems[$gemIndex] @ "\");", 0.5);
	schedule("remoteeval(2048, say(0, \"100\"));", 1);
	schedule("buy(\"" @ $gems[$gemIndex] @ "\");", 1.5);
	schedule("checkGems();", 1.5);
	schedule("buyGems();", 2);
}


	
function initSellGems(%gemIndex) {
	$gemIndex = %gemIndex;
	$continueSellingGems = true;
	schedule("remoteeval(2048, say(0, \"hi\"));", 0);
	schedule("remoteeval(2048, say(0, \"buy\"));", 1);
	schedule("sellGems();", 2);
}

function checkSellGems() {
	$currQuantity = getItemCount($gems[$gemIndex]);
	
	if($currQuantity == 0) {
		$continueSellingGems = false;
	}
}

function sellGems() {
	if(!$continueSellingGems) {
		echo("Done! " @ $gemQuantity);
		return;
	}
	remoteeval(2048, say(0, "100"));

	schedule("sell(\"" @ $gems[$gemIndex] @ "\");", 0.5);
	schedule("checkSellGems();", 0.5);
	schedule("sellGems();", 1);
}


$itemToSell = "";
$continueSellingItems = false;

function initSellItems(%itemToSell) {
	$itemToSell = %itemToSell;
	$continueSellingItems = true;
	sellItems();
}

function checkSellItems() {
	$currQuantity = getItemCount($itemToSell);
	
	if($currQuantity == 0) {
		$continueSellingItems = false;
	}
}

function sellItems() {
	if(!$continueSellingItems) {
		echo("Done!");
		return;
	}
	remoteeval(2048, say(0, "100"));

	schedule("sell(\"" @ $itemToSell @ "\");", 0.5);
	schedule("checkSellItems();", 1);
	schedule("sellItems();", 1.5);
}


Menu::New(JoundillMenu, "Joundill's script options!");
    Menu::AddChoice(JoundillMenu, "rStart AutoRemort", "AutoRemort::start();");
    Menu::AddChoice(JoundillMenu, "tStop AutoRemort", "AutoRemort::stop();");
    Menu::AddChoice(JoundillMenu, "pPathing", "Menu::Display(PathMenu);");
    Menu::AddChoice(JoundillMenu, "mStart Mining", "Mining::startMine(true);");
    Menu::AddChoice(JoundillMenu, "nStop Mining", "Mining::stopMine();");
    Menu::AddChoice(JoundillMenu, "sSell Gems (Keldrin)", "Mining::Keldrin::setup();");
    Menu::AddChoice(JoundillMenu, "xStop Sell Gems", "Mining::Keldrin::stop();");

bindCommand(keyboard0, make, shift, "j", TO, "Menu::Display(JoundillMenu);");
