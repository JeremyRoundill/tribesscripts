// Mug.cs

$Mug::itemToMug = "";
$Mug::mugging = false;
$Mug::delay = 3;

function Mug::startMug(%itemToMug) {
	$Mug::itemToMug = %itemToMug;
	$Mug::mugging = true;
	Mug::mug();
}


function Mug::stopMug() {
	$Mug::mugging = false;
}

function Mug::mug() {
	if(!$Mug::mugging) return;
	Actions::doAction("mug");
	schedule("remoteeval(2048, buy(\"" @ $Mug::itemToMug @ "\"));", 1.0);
	schedule("Mug::mug();", $Mug::delay);
}
