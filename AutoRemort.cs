// AutoRemort.cs

$AutoRemort::level = 0;
$AutoRemort::weaponType = "Slashing";
$AutoRemort::currentStep = "";

$AutoRemort::lastExp = 0;
$AutoRemort::exp = 0;

$AutoRemort::lastRemortTimestamp = "";

$AutoRemort::autoJoin = false;

function AutoRemort::init() {
	ChatEvents::Register("* has invited you to join his/her party.", "AutoRemort::autoJoinParty");
}

function AutoRemort::start() {
	$AutoRemort::autoJoin = true;
	$AutoRemort::lastExp = 0;
	Movement::init();
	Equipment::startDropTrash();
	Equipment::startStoreUseful();
	AutoRemort::regularCheck();
	Schedule::Add("AutoRemort::levelingCheck();", 33);
}

function AutoRemort::stop() {
	$AutoRemort::autoJoin = false;
	Equipment::stopDropTrash();
	Equipment::stopStoreUseful();
	Schedule::Cancel("AutoRemort::regularCheck();");
	Schedule::Cancel("AutoRemort::levelingCheck();");
	Message::clear();
}

function AutoRemort::regularCheck() {
	schedule("$AutoRemort::exp = DeusRPG::FetchData(\"EXP\");", 29);
	Schedule::Add("AutoRemort::regularCheck();", 30);

	$AutoRemort::level = DeusRPG::FetchData("LVL");
	$AutoRemort::remortLevel = DeusRPG::FetchData("RemortStep");
	$AutoRemort::exp = DeusRPG::FetchData("EXP");
	$AutoRemort::slashingLevel = DeusRPG::FetchData("skill " @ $SP::Position["Slashing"]);
	$AutoRemort::neutralCastingLevel = DeusRPG::FetchData("skill " @ $SP::Position["Neutral Casting"]);
	$AutoRemort::offensiveCastingLevel = DeusRPG::FetchData("skill " @ $SP::Position["Offensive Casting"]);

	AutoRemort::updateCentrePrint();
	Equipment::equipBest($AutoRemort::weaponType);

	if(DeusRPG::FetchData("Coins") > 40000) {
		say(2, "#dropcoins 10000");
	}

	echo("Current level: " @ $AutoRemort::level);

	// if($AutoRemort::level < 20) {
	// 	if($AutoRemort::currentStep != "Gran'Kar") {
	// 		Paths::stopAll();
	// 		$AutoRemort::currentStep = "Gran'Kar";
	// 		$AutoRemort::lastExp = 0;
	// 		Actions::stopMediCast();
	//		Actions::scheduleAction("wake", 4);

	// 		schedule("Paths::GranKar::recallAndStart();", 6);
	//		schedule("AutoRemort::updateCentrePrint();", 7);
	// 	}
	//	return;
	// }

	if($AutoRemort::slashingLevel < 600 && $AutoRemort::level < 40) {
		if($AutoRemort::currentStep != "Crypt") {
			Paths::stopAll();
			$AutoRemort::currentStep = "Crypt";
			Actions::stopMediCast();
			Actions::scheduleAction("wake", 4);

			// Open tab menu
			schedule("remoteeval(2048, ScoresOn);", 5);
			// Open SP page
			schedule("ClientMenuSelect(sp);", 6);
			schedule("SP::levelUpSkill(\"Sense Heading\", 30);", 7);
			schedule("SP::levelUpSkill(\"Healing\", 30);", 9);
			schedule("SP::levelUpSkill(\"Neutral Casting\", 30);", 11);
			schedule("SP::levelUpSkill(\"Endurance\", 30);", 13);
			schedule("Paths::Crypt::setup();", 15);
			schedule("AutoRemort::updateCentrePrint();", 16);

			Schedule::Cancel("AutoRemort::levelingCheck();");
			Schedule::Add("AutoRemort::levelingCheck();", 420);
		}

		return;
	}

	// if($AutoRemort::level < 75) {
	// 	if($AutoRemort::currentStep != "Minotaur") {
	//		Paths::stopAll();
	// 		$AutoRemort::currentStep = "Minotaur";
	// 		$AutoRemort::lastExp = 0;
	// 		Actions::stopMediCast();
 	// 		// Open tab menu
	// 		schedule("remoteeval(2048, ScoresOn);", 5);
	// 		// Open SP page
	// 		schedule("ClientMenuSelect(sp);", 6);
	// 		schedule("SP::levelUpSkill(\"Sense Heading\", 30);", 7);
	// 		schedule("SP::levelUpSkill(\"Healing\", 30);", 9);
	// 		schedule("Paths::Minotaur::setup();", 11);
	//		schedule("AutoRemort::updateCentrePrint();", 12);
	// 	}
	// 	return;
	// }

	if($AutoRemort::level < 101) {
		if($AutoRemort::currentStep != "Uber") {
			Paths::stopAll();
			$AutoRemort::currentStep = "Uber";
			$AutoRemort::lastExp = 0;
			Actions::stopMediCast();
			Actions::scheduleAction("wake", 4);
			// Open tab menu
			schedule("remoteeval(2048, ScoresOn);", 5);
			// Open SP page
			schedule("ClientMenuSelect(sp);", 6);
			schedule("SP::levelUpSkill(\"Sense Heading\", 30);", 7);
			schedule("SP::levelUpSkill(\"Healing\", 30);", 9);
			schedule("Paths::Uber::setup();", 11);
			schedule("AutoRemort::updateCentrePrint();", 12);

			Schedule::Cancel("AutoRemort::levelingCheck();");
			Schedule::Add("AutoRemort::levelingCheck();", 420);
		}
		return;
	}

	Actions::stopMediCast();
	Actions::scheduleAction("wake", 4);
	schedule("AutoRemort::remort();", 5);
}

// Sometimes we check if we're not gaining xp, and if we're not, reset the current step variable which will
// cause the regularCheck to reset our position, and hopefully get us back to gaining xp
function AutoRemort::levelingCheck() {
	$AutoRemort::exp = DeusRPG::FetchData("EXP");

	if($AutoRemort::lastExp == $AutoRemort::exp) {
		// TODO:: output here
		$AutoRemort::currentStep = "";
	}

	$AutoRemort::lastExp = $AutoRemort::exp;
	schedule("$AutoRemort::exp = DeusRPG::FetchData(\"EXP\");", 302);
	Schedule::Add("AutoRemort::levelingCheck();", 303);
}

function AutoRemort::castBestSpell() {
	%bestSpell = "fireball";
	if($AutoRemort::offensiveCastingLevel < 140) {
		%bestSpell = "firebomb";
	} else if ($AutoRemort::offensiveCastingLevel < 220) {
		%bestSpell = "cloud";
	} else {
		%bestSpell = "melt";
	}

	Actions::startMedicast(%bestSpell);
}


function AutoRemort::remort() {
	$AutoRemort::level = DeusRPG::FetchData("LVL");
	if($AutoRemort::level >= 145) {
		echo("Pretty sure we shouldn't be remorting this character");
		return;
	}
	Paths::stopAll();
	$AutoRemort::lastExp = 0;
	$AutoRemort::lastRemortTimestamp = timeStamp();

	say(2, "#cast remort");

	schedule("DeusRPG::FetchData(LVL);", 5);
	schedule("DeusRPG::FetchData(LVL);", 6);
	schedule("DeusRPG::FetchData(LVL);", 7);
	schedule("AutoRemort::remort2();", 8);

	Schedule::Cancel("AutoRemort::levelingCheck();");
	Schedule::Add("AutoRemort::levelingCheck();", 420);
}

function AutoRemort::remort2() {
	$AutoRemort::level = DeusRPG::FetchData("LVL");

	if($AutoRemort::level > 30) {
		AutoRemort::remort();
	}

	%delay = SP::spend();

	Schedule::Cancel("AutoRemort::regularCheck();");
	Schedule::Add("AutoRemort::regularCheck();", %delay);
}

function AutoRemort::updateCentrePrint() {
	$AutoRemort::message = "<jc>";
	$AutoRemort::message = $AutoRemort::message @ "<f0>Level: <f1>" @ $AutoRemort::level;
	$AutoRemort::message = $AutoRemort::message @ "<f0>, Remort Level: <f1>" @ $AutoRemort::remortLevel @ "\n";
	$AutoRemort::message = $AutoRemort::message @ "<f0>Slashing Level: <f1>" @ $AutoRemort::slashingLevel @ "\n";
	$AutoRemort::message = $AutoRemort::message @ "<f0>Offensive Casting Level: <f1>" @ $AutoRemort::offensiveCastingLevel @ "\n";

	if($Actions::mediCastingSpell != "") {
		$AutoRemort::message = $AutoRemort::message @ "<f0>Casting Spell: <f1>" @ $Actions::mediCastingSpell @ "\n";
	} else {
		$AutoRemort::message = $AutoRemort::message @ "<f0>Not currently casting\n";
	}

	$AutoRemort::message = $AutoRemort::message @ "<f0>Current area: <f1>" @ $AutoRemort::currentStep;

	if($Movement::Pathing) {
		$AutoRemort::message = $AutoRemort::message @ "<f0>, currently auto-moving";
	} else {
		$AutoRemort::message = $AutoRemort::message @ "<f0>, not currently auto-moving";
	}

	if($AutoRemort::lastRemortTimestamp != "") {
		$AutoRemort::message = $AutoRemort::message @ "\n<f0>Last remorted at <f1>" @ $AutoRemort::lastRemortTimestamp;
	}

	Message::print($AutoRemort::message);
}


function AutoRemort::autoJoinParty(%playerName) {
	if(!$AutoRemort::autoJoin) return;
	%inviterClientID = getClientByName(%playerName);
	remoteEval(2048, ScoresOn);
	schedule("clientMenuSelect(\"partyoptions\");", 0.5);
	schedule("clientMenuSelect(\"acceptinv " @ %inviterClientID @ "\");", 1.0);
}
