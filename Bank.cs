// Bank.cs

$Bank::Open = true;

function Bank::storeItem(%itemName) {
	%origItemCount = getItemCount(%itemName);
	sell(%itemName);
	Schedule::Add("Bank::storedSuccessfullyCheck(\"" @ %itemName @ "\", " @ %origItemCount @ ");", 1);
}

function Bank::storedSuccessfullyCheck(%itemName, %origItemCount) {
	%newItemCount = getItemCount(%itemName);
	$Bank::Open = %origItemCount != %newItemCount;
}

function Bank::retrieveItem(%itemName) {
	%origItemCount = getItemCount(%itemName);
	buy(%itemName);
	Schedule::Add("Bank::retrievedSuccessfullyCheck(\"" @ %itemName @ "\", " @ %origItemCount @ ");", 1);
}

function Bank::retrievedSuccessfullyCheck(%itemName, %origItemCount) {
	%newItemCount = getItemCount(%itemName);
	$Bank::Open = %origItemCount != %newItemCount;
	if(!$Bank::Open) remoteEval(2048, ToggleInventoryMode);
}