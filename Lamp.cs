// Lamp.cs

function Lamp::init() {
	ChatEvents::register("* says, \"clap\"", Lamp::on);
	ChatEvents::register("* says, \"clap clap\"", Lamp::off);
}

function Lamp::on() {
	// Equip Orb of Luminance
	useItem(73);
}

function Lamp::off() {
	// Unequip Orb of Luminance
	useItem(74);	
}