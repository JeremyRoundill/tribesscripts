// TvT:

bindCommand(keyboard0, make, ",", TO, "Equipment::equip(\"Beam Gun\");");
bindCommand(keyboard0, make, ".", TO, "Equipment::equip(\"Chain Saw\");");
bindCommand(keyboard0, make, "/", TO, "Equipment::equip(\"Mining Drill\");");

function TVT::startMine() {
	$TVT::mining = true;
	schedule("postaction(2048,IDACTION_BREAK1,1);", 0.5);
	schedule("postaction(2048,IDACTION_FIRE1,1);", 1);

	postaction(2048,IDACTION_MOVEFORWARD,1);
	postaction(2048,IDACTION_TURNLEFT,0.05);
	//schedule("TVT::mine();", 1.5);
}

function TVT::stopMine() {
	$TVT::mining = false;
	postaction(2048,IDACTION_MOVEFORWARD,0);
	postaction(2048,IDACTION_TURNLEFT,0);
	postaction(2048,IDACTION_BREAK1,1);
}

function TVT::mine() {
	if(!$TVT::mining) return;
	// TODO: add in here a stop & start swinging
	schedule("postaction(2048,IDACTION_MOVEFORWARD,1);", 0);
	schedule("postaction(2048,IDACTION_MOVEFORWARD,0);", 1);
	schedule("postaction(2048,IDACTION_MOVEBACK,1);", 4);
	schedule("postaction(2048,IDACTION_MOVEBACK,0);", 5);
	schedule("TVT::mine();", 8);
}

function TVT::startAutoAttack() {
	$TVT::attacking = true;
	TVT::autoAttack();
}

function TVT::stopAutoAttack() {
	$TVT::attacking = false;
}

function TVT::autoAttack() {
	if(!$TVT::attacking) return;
	postAction(2048, IDACTION_BREAK1, 1);
	schedule("postAction(2048, IDACTION_FIRE1, 1);", 0.3);
	schedule("TVT::autoAttack();", 0.6);
}

function TVT::breakMakeSell() {
	Schedule::add("TVT::breakAllWood();", 0);
	Schedule::add("TVT::makeBasicArrows();", 300);
	Schedule::add("TVT::startSellBasicArrows();", 600);
	Schedule::add("TVT::breakMakeSell();", 900);
}

function TVT::stopBreakMakeSell() {
	Schedule::cancel("TVT::breakAllWood();");
	Schedule::cancel("TVT::makeBasicArrows();");
	Schedule::cancel("TVT::startSellBasicArrows();");
	Schedule::cancel("TVT::breakMakeSell();");	
}

function TVT::breakAllWood() {
	if(getItemCount("WormWood") > 0) {
		TVT::breakWood("WormWood", "TVT::breakAllWood();");
	} else if(getItemCount("BireWood") > 0) {
		TVT::breakWood("BireWood", "TVT::breakAllWood();");
	} else if(getItemCount("PineWood") > 0) {
		TVT::breakWood("PineWood", "TVT::breakAllWood();");
	} else if(getItemCount("OakWood") > 0) {
		TVT::breakWood("OakWood", "TVT::breakAllWood();");
	} else if(getItemCount("Lumber") > 0) {
		TVT::breakWood("Lumber", "TVT::breakAllWood();");
	} else if(getItemCount("LongRod") > 0) {
		TVT::breakWood("LongRod", "TVT::breakAllWood();");		
	} else if(getItemCount("Rod") > 0) {
		TVT::breakWood("Rod", "TVT::breakAllWood();");		
	} else if(getItemCount("Stick") > 0) {
		TVT::breakWood("Stick", "TVT::breakAllWood();");		
	}
}

function TVT::breakWood(%woodName, %callback) {
	%quantity = getItemCount(%woodName);
	if(%quantity > 250) %quantity = 250;
	if(%quantity == 0) return;
	say(1, "#break " @ %woodName @ " " @ %quantity);
	schedule(%callback, 0.4);
}


function TVT::dropAllWood() {
	if(getItemCount("WormWood") > 0) {
		TVT::dropWood("WormWood", "TVT::dropAllWood();");
	} else if(getItemCount("BireWood") > 0) {
		TVT::dropWood("BireWood", "TVT::dropAllWood();");
	} else if(getItemCount("PineWood") > 0) {
		TVT::dropWood("PineWood", "TVT::dropAllWood();");
	} else if(getItemCount("OakWood") > 0) {
		TVT::dropWood("OakWood", "TVT::dropAllWood();");
	} else if(getItemCount("Lumber") > 0) {
		TVT::dropWood("Lumber", "TVT::dropAllWood();");
	} else if(getItemCount("LongRod") > 0) {
		TVT::dropWood("LongRod", "TVT::dropAllWood();");		
	} else if(getItemCount("Rod") > 0) {
		TVT::dropWood("Rod", "TVT::dropAllWood();");		
	} else if(getItemCount("Stick") > 0) {
		TVT::dropWood("Stick", "TVT::dropAllWood();");		
	} else if(getItemCount("Twig") > 0) {
		TVT::dropWood("Twig", "TVT::dropAllWood();");		
	}
}

function TVT::dropWood(%woodName, %callback) {
	%quantity = getItemCount(%woodName);
	if(%quantity == 0) return;
	say(1, "500");
	drop(%woodName);
	schedule(%callback, 0.4);
}

function TVT::makeBasicArrows() {
	%twigCount = getItemCount(Twig);
	if(%twigCount == 0) {
		Message::print("<jc><f0>No twigs remaining", 5);
		return;
	}
	if(%twigCount > 500) %twigCount = 500;

	say(1, "#smith basicarrow " @ %twigCount);
	schedule("TVT::makeBasicArrows();", 0.4);
}

function TVT::dropBasicArrows() {
	%arrowCount = getItemCount("Basic Arrow");
	if(%arrowCount == 0) {
		Message::print("<jc><f0>No basic arrows remaining", 5);
		return;
	}
	if(%arrowCount > 500) %arrowCount = 500;

	say(2, %arrowCount);
	drop("Basic Arrow");
	schedule("TVT::dropBasicArrows();", 0.4);
}

function TVT::startSellBasicArrows() {
	$TVT::sellingBasicArrows = true;
	Actions::scheduleAction("say hi", 0);
	Actions::scheduleAction("say buy", 0.4);
	schedule("TVT::sellBasicArrows();", 0.8);

}

function TVT::sellBasicArrows() {
	if(!$TVT::sellingBasicArrows || getItemCount("Basic Arrow") <= 0) return;
	say(1, "2000");
	schedule("sell(\"Basic Arrow\");", 0.4);
	schedule("TVT::sellBasicArrows();", 0.8);
}

$TVT::foods["bread"] = 50;
$TVT::foods["pie"] = 100;
$TVT::foods["honeypie"] = 120;
$TVT::foods["strawberrypie"] = 180;

function TVT::startEat(%food) {
	$TVT::food = %food;
	$TVT::eating = true;
	TVT::eat();
}


function TVT::stopEat() {
	$TVT::eating = false;
}

function TVT::eat() {
	if(!$TVT::eating) return;
	%hp = DeusRPG::FetchData("HP");
	%maxhp = DeusRPG::FetchData("MaxHP");

	%diff = $TVT::foods[$TVT::food];
	if(%diff == "" || %diff == -1) %diff = 200;

	if(%maxhp - %hp >= %diff) {
		say(2,"#eat " @ $TVT::food);
	}
	schedule("TVT::eat();", 0.5);
}

function TVT::startWeightTraining() {
	$TVT::weightTraining = true;
	postaction(2048,IDACTION_MOVEFORWARD,1);

	DeusRPG::FetchData("Weight");
	DeusRPG::FetchData("MaxWeight");

	schedule("TVT::weightTraining();", 1);
	schedule("TVT::checkWeight();", 1);
	TVT::tracking();
}

function TVT::stopWeightTraining() {
	$TVT::weightTraining = false;	
	postaction(2048,IDACTION_MOVEFORWARD,0);
	TVT::stopTracking();
}

function TVT::weightTraining() {
	if(!$TVT::weightTraining) return;
	postaction(2048,IDACTION_MOVEUP,1);
	schedule("TVT::weightTraining();", 0.1);
}

function TVT::tracking() {
	say(2, "#compass");
	Schedule::Add("TVT::tracking();", 0.5);
}

function TVT::stopTracking() {
	Schedule::Cancel("TVT::tracking();");
}

function TVT::checkWeight() {
	if(!$TVT::weightTraining) return;

	%weight = DeusRPG::FetchData("Weight");
	%maxWeight = DeusRPG::FetchData("MaxWeight");

	// coin = 0.001 weight
	%difference = %maxWeight - %weight;
	if(%difference >= 1) {
		TVT::stopTracking();

		%amountToWithdraw = Floor(%difference * 1000) - 100;
		Actions::scheduleAction("say hi", 0);
		Actions::scheduleAction("say withdraw", 1);
		Actions::scheduleAction("say " @ %amountToWithdraw, 2);

		Schedule::Add("TVT::tracking();", 3);
	} else if (%difference <= 0) {
		TVT::stopTracking();

		%amountToDeposit =  Floor(0 - %difference * 1000) + 100;
		Actions::scheduleAction("say hi", 0);
		Actions::scheduleAction("say deposit", 1);
		Actions::scheduleAction("say " @ %amountToDeposit, 2);

		Schedule::Add("TVT::tracking();", 3);
	}

	schedule("DeusRPG::FetchData(\"Weight\");", 3);
	schedule("DeusRPG::FetchData(\"MaxWeight\");", 3);
	schedule("TVT::checkWeight();", 4);
}

function TVT::fixTreeBoss() {
	postAction(2048, IDACTION_LOOKDOWN, 1);
}

function TVT::unfixTreeBoss() {
	postAction(2048, IDACTION_LOOKDOWN, 0);
}

function TVT::plantTrees() {
	if(getItemCount("TreeFruit") <= 0) return;
	drop("TreeFruit");
	Schedule::Add("TVT::plantTrees();", 0.1);
}


function TVT::stopPlantTrees() {
	Schedule::Cancel("TVT::plantTrees();");
}

function TVT::chainsaw() {
	echo("Chainsaw!");
	Equipment::equip("Chain Saw");
	Actions::startAutoAttack();
}
editActionMap("playmap.sae");
bindCommand(keyboard0, make, shift, "1", TO, "TVT::chainsaw();");
bindCommand(keyboard0, make, shift, "2", TO, "say(2, \"#cast parchment\");");

