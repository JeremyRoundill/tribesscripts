// Mining.cs

// Include("Jeremy\\Whitelist.cs");
$i = -1;
$Whitelist::users[$i++] = "Joundill";
$Whitelist::users[$i++] = "JoundillDruid";
$Whitelist::users[$i++] = "NewToon";
$Whitelist::users[$i++] = "SIayer";
$Whitelist::users[$i++] = "Caboose";
$Whitelist::users[$i++] = "DaThief";
$Whitelist::users[$i++] = "Baphomet";
$Whitelist::users[$i++] = "Thatoneguy";
$Whitelist::users[$i++] = "jonathan";
$Whitelist::users[$i++] = "PBDeath666";
$Whitelist::users[$i++] = "Leviathan";
$Whitelist::users[$i++] = "Belial";
$Whitelist::users[$i++] = "WisCorp";
$Whitelist::users[$i++] = "Asmodeus";
$Whitelist::users[$i++] = "Behemoth";
$Whitelist::users[$i++] = "Saibot";
$Whitelist::users[$i++] = "Deathchild";
$Whitelist::users[$i++] = "DeathChild";
$Whitelist::users[$i++] = "deathchild";
$Whitelist::users[$i++] = "Malice";
$Whitelist::users[$i++] = "Verdict";
$Whitelist::users[$i++] = "Suroot";
$Whitelist::users[$i++] = "Shslappy";
$Whitelist::users[$i++] = "Aethen3";

// Constants
$Mining::moveTime = 0.1; // how long to move forward/backward
$Mining::moveDelay = 0.9; // how long in between steps
$Mining::wwsEquipped = true;
$Mining::hammerHidden = false;

$i = -1;
$Mining::gems[$i++] = "Small rock";
$Mining::gems[$i++] = "Quartz";
$Mining::gems[$i++] = "Granite";
$Mining::gems[$i++] = "Opal";
$Mining::gems[$i++] = "Jade";
$Mining::gems[$i++] = "Turquoise";
$Mining::gems[$i++] = "Ruby";
$Mining::gems[$i++] = "Topaz";
$Mining::gems[$i++] = "Sapphire";
$Mining::gems[$i++] = "Gold";
$Mining::gems[$i++] = "Emerald";
$Mining::gems[$i++] = "Diamond";
$Mining::gems[$i++] = "Iron";
$Mining::gems[$i++] = "Silver";
$Mining::gems[$i++] = "Titanium";
$Mining::gems[$i++] = "Nickel";

// Variables
$Mining::mining = false; // is currently mining
$Mining::moving = false; // is currently moving
$Mining::moveForward = false; // move forwards or backwards?
$Mining::thorrMining = true; // is switching WWs to make thorr hammer mining faster
$Mining::steps = 0; // number of steps taken so far
$Mining::previousFound = ""; // second most recent item found
$Mining::lastFound = ""; // most recent item found

$Mining::failedChecks = 0; // how many times we've failed to find gems in a row

$Mining::userName = "default";


// Sets up and starts mining
function Mining::startMine(%useThorr) {
	$Mining::thorrMining = %useThorr;
	Mining::initGemCounts();

	$Mining::mining = true;
	$Mining::moveForward = false;
	$Mining::steps = 0;
	$Mining::previousFound = "";
	$Mining::lastFound = "";
	$Mining::moving = true;
	
	if(getItemCount("Thorr's Hammer") > 0) {
		$Mining::hammerHidden = false;
	} else {
		$Mining::hammerHidden = true;
	}
	
	// Start swinging
	postaction(2048,IDACTION_BREAK1,1);
	schedule("postaction(2048,IDACTION_FIRE1,1);", 0.5);
	Mining::move();
	Mining::bankGems();
	
	// Equip Wind Walkers
	remoteEval(2048,useItem,21);
	// Register chat watcher for if an item was received
	ChatEvents::register("You found *.", Mining::foundGem);
}

// Stop mining
function Mining::stopMine() {
	// TODO: Deregister currently doesn't work.
	// ChatEvents::deregister("You found *.");
	// ChatEvents::init();
	Mining::stopThorrMine();
	$Mining::mining = false;
}

// Run backwards into the corner, then move forwards to the gem spike and start mining again
function Mining::resetMine(%time) {
	// TODO: add in here a stop & start swinging
	schedule("postaction(2048,IDACTION_MOVEBACK,1);", %time);
	schedule("postaction(2048,IDACTION_BREAK1,1);", %time + 1);
	schedule("postaction(2048,IDACTION_MOVEBACK,0);", %time + 1.5);
	schedule("postaction(2048,IDACTION_MOVEFORWARD,1);", %time + 1.5);
	schedule("postaction(2048,IDACTION_FIRE1,1);", %time + 2);
	schedule("postaction(2048,IDACTION_MOVEFORWARD,0);", %time + 2.3);
	return %time + 2.5;
}

// Callback for when an item is received
function Mining::foundGem(%gem) {
	// Save the time the last gem was received
	$Mining::lastFound = TimeStamp();
}

function Mining::move() {
	// If we're not mining, give up
	if(!$Mining::mining) return;

	// Check if we've found gems
	if($Mining::previousFound == $Mining::lastFound) {
		// If we're not finding gems, begin to move and increment the number of failed
		// checks
		$Mining::failedChecks = $Mining::failedChecks + 1;
		$Mining::moving = true;
	} else {
		// If we are finding gems, don't move and reset the number of failed checks
		$Mining::failedChecks = 0;
		$Mining::moving = false;
	}
	// Update the previous found time to the current last found time
	$Mining::previousFound = $Mining::lastFound;
	
	// If we fail to find gems enough times, reset our mining position
	if($Mining::failedChecks > 10) {
		$Mining::failedChecks = 0;
		%resetTime = Mining::resetMine(0);
		schedule("Mining::move();", %resetTime);
		return;
	}
	
	if($Mining::moving) {
		// If we're moving forward, go forward. Backward, go back.
		if($Mining::moveForward) {
			postaction(2048,IDACTION_MOVEFORWARD,1);
			schedule("postaction(2048,IDACTION_MOVEFORWARD,0);", $Mining::moveTime);
			$Mining::steps++;
		} else {
			postaction(2048,IDACTION_MOVEBACK,1);
			schedule("postaction(2048,IDACTION_MOVEBACK,0);", $Mining::moveTime);
			$Mining::steps++;
		}
		
		// Change direction every few steps so we don't get too far away from the
		// gem spike
		if($Mining::steps > 4) {
			echo("changing direction");
			$Mining::steps = 0;
			$Mining::moveForward = !$Mining::moveForward;
		}
	}

	// If we're fast swinging, then equip/unequip wws to get speed up
	if($Mining::thorrMining) {
		if(getItemCount("Wind Walkers") == 0) {
			schedule("useItem(22);", $Mining::moveDelay/2); 
		} else {
			schedule("useItem(21);", $Mining::moveDelay/2); 
		}

		// Deposit Wind Walkers if we've got multiple pairs
		if(getItemCount("Wind Walkers") > 1) {
			sell("Wind Walkers");
		}
		$Mining::wwsEquipped = !$Mining::wwsEquipped;
	}
	
	// Lesgoooooo
	schedule("Mining::move();", $Mining::moveDelay);
}

// Put gems in storage when we get a stack of 100
function Mining::bankGems() {
	// If we're not mining, give up
	if(!$Mining::mining) return;
	
	%i = 0;
	%gem = $Mining::gems[%i];
	// Loop through all the gems we know about
	while(%gem != -1 && %gem != "") {
		
		// If we've got more than 100 of that gem, deposit it
		%gemCount = getItemCount(%gem);
		if(%gemCount >= 100) {
			say(0, "100");
			sell(%gem);
			Mining::recordGemsDeposited(%gem, 100);
			break;
		}
		
		%i = %i + 1;
		%gem = $Mining::gems[%i];
	}
	
	schedule("Mining::bankGems();", 4);
}


function Mining::initGemCounts() {
	$Mining::userName = Client::getName(getManagerId());
	Include("MiningData" @ $Mining::userName @ ".cs");
}

function Mining::recordGemsDeposited(%gem, %amount) {
	if($Mining::gemsDeposited == -1) {
		echo("Couldn't record gem deposit");
	}

	if($Mining::gemsDeposited[%gem] == -1) {
		$Mining::gemsDeposited[%gem] = 0;
	} 

	$Mining::gemsDeposited[%gem] += %amount;

	export("$Mining::gemsDeposited*", "config\\MiningData" @ $Mining::userName @ ".cs");
}

function Mining::printDepositedGems() {

	%message = "<f1>You've mined:\n";
 	for(%i = 1; $Mining::gems[%i] != ""; %i++) {
 		%gem = $Mining::gems[%i];
 		%amount = $Mining::gemsDeposited[%gem];
 		if(%amount != "") {
 			%message = %message @ "<f1>" @ %gem @ ": <f0>" @ %amount @ "\n";
 		}
 	}

    Message::print(%message, 15);

}

function Mining::clientUpdate(%client) {
	// If we're not mining, who cares?
	if(!$Mining::mining) return;
	// If this is a bot joining, don't bother doing anything
	if(Client::GetTeam(%client) != 0) return;

	%hideHammer = false;

	for(%i = 2833; %i > 2048; %i--) {
		%playerName = Client::getName(%i);
		%team = Client::GetTeam(%i);

		// If not on team citizen, continue
		if(%team != 0) continue;

		if(%playerName != "") {
			%trusted = false;
			for(%j = 0; $Whitelist::users[%j] != ""; %j++) {
				%whitelistedUser = $Whitelist::users[%j];
				if(%playerName == %whitelistedUser) {
					%trusted = true;
					break;
				}		
			}

			if(!%trusted) {
			 	%hideHammer = true;
			 	break;
			}
		}
	}


	if(%hideHammer) {
		echo(%playerName @ " is not on the whitelist, hiding hammer");
		Mining::hideHammer();
	} else {
		Mining::retrieveHammer();
	}
}

function Mining::hideHammer() {
	if(getItemCount("Thorr's Hammer") == 0) return;
	$Mining::thorrMining = false;

	if(getItemCount("Pick Axe") == 0) {
		schedule("buy(\"Pick Axe\");", 1);
		schedule("use(\"Pick Axe\");", 2);
	}

	// In case we ended up with more than one somehow
	for(%i = 0; %i < getItemCount("Thorr's Hammer"); %i++) {
		schedule("sell(\"Thorr's Hammer\");", 2 + %i);
	}

	schedule("use(\"Pick axe\");", 6 + %i);
	schedule("postaction(2048,IDACTION_BREAK1,1);", 7 + %i);
	schedule("postaction(2048,IDACTION_FIRE1,1);", 8 + %i);
	schedule("$Mining::thorrMining = true;", 9 + %i);
}

function Mining::retrieveHammer() {
	if(getItemCount("Thorr's Hammer") >= 1) return;
	$Mining::thorrMining = false;

	if(getItemCount("Thorr's Hammer") == 0) {
		schedule("buy(\"Thorr's Hammer\");", 1);
		schedule("use(\"Thorr's Hammer\");", 2);
	}


	// In case we ended up with more than one somehow
	for(%i = 0; %i < getItemCount("Pick Axe"); %i++) {
		schedule("sell(\"Pick Axe\");", 2 + %i);
	}

	schedule("use(\"Thorr's Hammer\");", 6 + %i);
	schedule("postaction(2048,IDACTION_BREAK1,1);", 7 + %i);
	schedule("postaction(2048,IDACTION_FIRE1,1);", 8 + %i);
	schedule("$Mining::thorrMining = true;", 9 + %i);
}

Event::Attach(eventClientJoin, Mining::clientUpdate);
Event::Attach(eventClientDrop, Mining::clientUpdate);
Event::Attach(eventClientChangeTeam, Mining::clientUpdate);
Event::Attach(eventClientTeamAdd, Mining::clientUpdate);




function Mining::Ethren::startSellGems() {
	$Mining::sellingGems = true;
	Movement::smartTurnAngle(-$pi/12, "sellGems();");
}

function Mining::Ethren::withdrawGems() {

}

function Mining::Ethren::sellGems(%cb) {
	Mining::Ethren::moveToMerchant();
}

function Mining::Ethren::moveToMerchant(%cb) {
	if(!$Mining::sellingGems) return;
	postaction(2048, IDACTION_MOVEBACK, 1);
	schedule("postaction(2048, IDACTION_MOVEBACK, 0);", 3);
	schedule(%cb, 3);
}

function Mining::Ethren::moveToBanker(%cb) {
	if(!$Mining::sellingGems) return;
	postaction(2048, IDACTION_MOVEFORWARD, 1);
	schedule("postaction(2048, IDACTION_MOVEFORWARD, 0);", 3);
	schedule(%cb, 3);
}


$Mining::Keldrin::selling = false;
function Mining::Keldrin::setup() {

	$Mining::Keldrin::selling = true;
	deleteVariables("$Mining::gemsInBank*");
 	for(%i = 0; $Mining::gems[%i] != ""; %i++) {
		%gem = $Mining::gems[%i];
		$Mining::gemsInBank[%gem] = true;
	}

	DeusRPG::FetchData("Weight");
	DeusRPG::FetchData("MaxWeight");

	Movement::smartTurnCardinal(NE, "Mining::Keldrin::moveToMerchant();"); // Change cardinal here
}

function Mining::Keldrin::stop() {
	$Mining::Keldrin::selling = false;
}

function Mining::Keldrin::moveToMerchant() {
	if(!$Mining::Keldrin::selling) return;
	Movement::cardinalMove(NE, 1); // Change cardinal
	schedule("Mining::Keldrin::openShop();", 2);
}

function Mining::Keldrin::openShop() {
	if(!$Mining::Keldrin::selling) return;
	Movement::cardinalMove(E, 1);
	Actions::scheduleAction("say hi", 0);
	Actions::scheduleAction("say buy", 2);
	schedule("Mining::Keldrin::moveToBank();", 2);
}

function Mining::Keldrin::moveToBank() {
	if(!$Mining::Keldrin::selling) return;
	Movement::cardinalMove(SW, 1); // Change cardinal
	schedule("Mining::Keldrin::sellGems();", 1);
	Movement::smartTurnCardinal(NE); // In case we've looked the wrong way by accident
}

function Mining::Keldrin::sellGems() {
	if(!$Mining::Keldrin::selling) return;
	%soldGem = false;
 	for(%i = 0; $Mining::gems[%i] != ""; %i++) {
		%gem = $Mining::gems[%i];
		
		// If we've got more than 100 of that gem, deposit it
		%gemCount = getItemCount(%gem);
		if(%gemCount > 0) {
			say(2, "100");
			sell(%gem);
			%soldGem = true;
			break;
		}
	}

	if(%soldGem) {
		schedule("Mining::Keldrin::sellGems();", 1);
		return;
	}

	schedule("Mining::Keldrin::depositCoins();", 1);
}

function Mining::Keldrin::depositCoins() {
	if(!$Mining::Keldrin::selling) return;
	Actions::scheduleAction("say hi", 0);
	Actions::scheduleAction("say deposit", 2);
	Actions::scheduleAction("say all", 4);
	schedule("Mining::Keldrin::openStorage();", 6);
}

function Mining::Keldrin::openStorage() {
	Actions::scheduleAction("say hi", 0);
	Actions::scheduleAction("say storage", 2);
	schedule("Mining::Keldrin::buyGems();", 4);
}


function Mining::Keldrin::buyGems() {
	if(!$Mining::Keldrin::selling) return;
	%boughtGem = true;
	%weight = DeusRPG::FetchData("Weight");
	%maxWeight = DeusRPG::FetchData("MaxWeight");
	echo(%weight @ "/" @ %maxWeight);

	if(%weight > %maxWeight - 500) {
		Mining::Keldrin::moveToMerchant();
		return;
	}

 	for(%i = 0; $Mining::gems[%i] != ""; %i++) {
		%gem = $Mining::gems[%i];

		if(!$Mining::gemsInBank[%gem]) continue;
		echo("Doing gem " @ %gem);
		
		// If we've got more than 100 of that gem, deposit it
		%gemCount = getItemCount(%gem);

		if(%gemCount >= 20000) {
			// Just sell if we've got more than 20k, it's not worth the risk of a D/C
			Mining::Keldrin::moveToMerchant();
			return;
		}

		say(2, "100");
		schedule("buy(\"" @ %gem @ "\");", 0);
		schedule("say(2, 100);", 0.8);
		schedule("buy(\"" @ %gem @ "\");", 0.8);
		schedule("say(2, 100);", 1.6);
		schedule("buy(\"" @ %gem @ "\");", 1.6);
		schedule("say(2, 100);", 2.4);
		schedule("buy(\"" @ %gem @ "\");", 2.4);
		schedule("Mining::Keldrin::checkGemCount(\"" @ %gem @ "\", " @ %gemCount @ ");", 3.2);

		break;	
	}
	
}

function Mining::Keldrin::checkGemCount(%gem, %originalCount) {
	%newGemCount = getItemCount(%gem);

	if(%originalCount == %newGemCount) {
		$Mining::gemsInBank[%gem] = false;
		echo(%gem @ " not in bank");
	}

	Mining::Keldrin::buyGems();
}



// Mining formula:

// %lastscore = "";
// 	for(%i = 1; $ItemList[Mining, %i] != ""; %i++)
// 	{
// 		%w1 = GetWord($ItemList[Mining, %i], 1) - %crystal.bonus[%i];
// 		%n = Cap( (%w1 * getRandom()) + (%w1 / 2), 0, %w1);
// 		%r = 1 + ($PlayerSkill[%clientId, $SkillMining] * (1/10)) * getRandom();

// 		if(%n > %r)
// 			return %lastscore;

// 		%lastscore = GetWord($ItemList[Mining, %i], 0);
// 	}
// 	return %lastscore;


// %w1 = 2913 - crystal bonus (assuming 0)
// maximum %n is 2913
// %n <= 1 + mining/10 * random
// 2913 - 1 <= mining/10 * random
// 29120 <= mining * random

// misses will start when random is 1

