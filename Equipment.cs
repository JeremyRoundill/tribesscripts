// Equipment.cs

// odd = equip
// even = unequip
// 11, 12 = Cheetaur's Paws
// 17, 18 = Titanium Braced Boots
// 19, 20 = Boots of Gliding
// 21, 22 = Wind Walkers
// 25, 26 = Steel Helmet
// 29, 30 = Milled-Steel Bracers
// 35, 36 = Fingerless Gloves
// 73, 74 = Orb of Luminance


// Weapon damage formula:
// ((WATK + RATK) / 1000 * skill * modifiers) - (rand*DEF/10)+1
// Plus random damage modifier, round(rand x (%value x 0.15) x 2 - %value)
// WATK = weapon attack
// RATK = ammo attack


// Weapons
//
// Slashing

$Equipment::droppingTrash = false;
$Equipment::storingUseful = false;

function Equipment::equip(%equipmentName) {
	useItem(getItemType(%equipmentName));
}

function Equipment::equipBest(%class) {
	%bestName = "";
	%bestDPS = 0;
	DeusRPG::FetchData("skill " @ $Equipment::SkillIndexes[%class]);
	%playerSkill = DeusRPG::FetchData("skill " @ $Equipment::SkillIndexes[%class]);
	for(%i = 0; $Equipment::weapons[%class, %i, Name] != ""; %i++) {
		%name = $Equipment::weapons[%class, %i, Name];
		%skill = $Equipment::weapons[%class, %i, Skill];
		%dps = $Equipment::weapons[%class, %i, ATK] / $Equipment::Weapons[%class, %i, Delay];

		if(%dps > %bestDPS && %playerSkill >= %skill && getItemCount(%name) > 0) {
			%bestDPS = %dps;
			%bestName = %name;
		}
	}

	echo("Best: " @ %bestName);

	Equipment::equip(%bestName);
}

function Equipment::startDropTrash() {
	$Equipment::droppingTrash = true;
	Equipment::dropTrash();
}

function Equipment::stopDropTrash() {
	$Equipment::droppingTrash = false;
}

function Equipment::dropTrash() {
	if(!$Equipment::droppingTrash) return;
	for(%i = 0; $Equipment::Trash[%i] != ""; %i++) {
		%trashName = $Equipment::Trash[%i];
		if(getItemCount(%trashName) > 0) {
			if(%trashName == "Rusty Long Sword" && getItemCount(%trashName) == 1) continue;

			drop(%trashName);
			schedule("Equipment::dropTrash();", 0.5);
			return;
		}
	}
	schedule("Equipment::dropTrash();", 5);
}

function Equipment::startStoreUseful() {
	$Equipment::storingUseful = true;
	Equipment::StoreUseful();
}

function Equipment::stopStoreUseful() {
	$Equipment::storingUseful = false;
}

function Equipment::storeUseful() {
	if(!$Equipment::storingUseful) return;
	for(%i = 0; $Equipment::Useful[%i] != ""; %i++) {
		%usefulName = $Equipment::Useful[%i];
		if(getItemCount(%usefulName) > 1) {
			Bank::storeItem(%usefulName);
			schedule("Equipment::storeUseful();", 2);
			return;
		}
	}

	if(!$Bank::Open) {
		schedule("Equipment::storeUseful();", 30);
		return;
	}

	schedule("Equipment::storeUseful();", 5);
}

function Equipment::startDropAll(%itemName) {
	$Equipment::itemToDrop = %itemName;
	Equipment::dropAll();
}

function Equipment::stopDropAll() {
	Schedule::Cancel("Equipment::dropAll();");
}

function Equipment::dropAll() {
	if(getItemCount($Equipment::itemToDrop) > 0) {
		drop($Equipment::itemToDrop);
		Schedule::Add("Equipment::dropAll();", 1);
	}
}

$Equipment::SkillIndexes[Slashing] = 1;
$Equipment::SkillIndexes[Piercing] = 2;
$Equipment::SkillIndexes[Bludgeoning] = 1;
$Equipment::SkillIndexes[Archery] = 14;

%i = -1;
$Equipment::Trash[%i++] = "Rusty Broad Sword";
$Equipment::Trash[%i++] = "Rusty War Axe";
$Equipment::Trash[%i++] = "Rusty Long Sword";
$Equipment::Trash[%i++] = "Cracked Club";
$Equipment::Trash[%i++] = "Cracked Spiked Club";
$Equipment::Trash[%i++] = "Rusty Knife";
$Equipment::Trash[%i++] = "Rusty Short Sword";
$Equipment::Trash[%i++] = "Rusty Pick Axe";
$Equipment::Trash[%i++] = "Cracked Short Bow";
$Equipment::Trash[%i++] = "Cracked Light Crossbow";
$Equipment::Trash[%i++] = "Sling";
$Equipment::Trash[%i++] = "Small Rock";
$Equipment::Trash[%i++] = "Basic Arrow";
$Equipment::Trash[%i++] = "Sheaf Arrow";
$Equipment::Trash[%i++] = "Blue Potion";


$Equipment::Trash[%i++] = "Granite";
$Equipment::Trash[%i++] = "Opal";
$Equipment::Trash[%i++] = "Skeleton Bone";


%i = -1;
$Equipment::Useful[%i++] = "Gladius";
$Equipment::Useful[%i++] = "Claymore";
$Equipment::Useful[%i++] = "Battle Axe";
$Equipment::Useful[%i++] = "Turquoise";
$Equipment::Useful[%i++] = "Diamond";
$Equipment::Useful[%i++] = "Gold";
$Equipment::Useful[%i++] = "Dragon Scale";
$Equipment::Useful[%i++] = "Enchanted Stone";



%i = 0;
$Equipment::Weapons[Slashing, %i, Name]  = "Hatchet";
$Equipment::Weapons[Slashing, %i, Skill] = 0;
$Equipment::Weapons[Slashing, %i, ATK]   = 25;
$Equipment::Weapons[Slashing, %i, Delay] = 1.666;

%i++;
$Equipment::Weapons[Slashing, %i, Name]  = "Broad Sword";
$Equipment::Weapons[Slashing, %i, Skill] = 20;
$Equipment::Weapons[Slashing, %i, ATK]   = 35;
$Equipment::Weapons[Slashing, %i, Delay] = 1.666;

%i++;
$Equipment::Weapons[Slashing, %i, Name]  = "Rusty Broad Sword";
$Equipment::Weapons[Slashing, %i, Skill] = 20;
$Equipment::Weapons[Slashing, %i, ATK]   = 24;
$Equipment::Weapons[Slashing, %i, Delay] = 2.5;

%i++;
$Equipment::Weapons[Slashing, %i, Name]  = "War Axe";
$Equipment::Weapons[Slashing, %i, Skill] = 60;
$Equipment::Weapons[Slashing, %i, ATK]   = 70;
$Equipment::Weapons[Slashing, %i, Delay] = 2.333;

%i++;
$Equipment::Weapons[Slashing, %i, Name]  = "Rusty War Axe";
$Equipment::Weapons[Slashing, %i, Skill] = 60;
$Equipment::Weapons[Slashing, %i, ATK]   = 49;
$Equipment::Weapons[Slashing, %i, Delay] = 3.5;

%i++;
$Equipment::Weapons[Slashing, %i, Name]  = "Long Sword";
$Equipment::Weapons[Slashing, %i, Skill] = 140;
$Equipment::Weapons[Slashing, %i, ATK]   = 65;
$Equipment::Weapons[Slashing, %i, Delay] = 1.666;

%i++;
$Equipment::Weapons[Slashing, %i, Name]  = "Rusty Long Sword";
$Equipment::Weapons[Slashing, %i, Skill] = 140;
$Equipment::Weapons[Slashing, %i, ATK]   = 45;
$Equipment::Weapons[Slashing, %i, Delay] = 2.5;

%i++;
$Equipment::Weapons[Slashing, %i, Name]  = "Battle Axe";
$Equipment::Weapons[Slashing, %i, Skill] = 300;
$Equipment::Weapons[Slashing, %i, ATK]   = 144;
$Equipment::Weapons[Slashing, %i, Delay] = 3;

%i++;
$Equipment::Weapons[Slashing, %i, Name]  = "Bastard Sword";
$Equipment::Weapons[Slashing, %i, Skill] = 620;
$Equipment::Weapons[Slashing, %i, ATK]   = 133;
$Equipment::Weapons[Slashing, %i, Delay] = 2.333;

%i++;
$Equipment::Weapons[Slashing, %i, Name]  = "Halberd";
$Equipment::Weapons[Slashing, %i, Skill] = 768;
$Equipment::Weapons[Slashing, %i, ATK]   = 176;
$Equipment::Weapons[Slashing, %i, Delay] = 2.666;

%i++;
$Equipment::Weapons[Slashing, %i, Name]  = "Claymore";
$Equipment::Weapons[Slashing, %i, Skill] = 900;
$Equipment::Weapons[Slashing, %i, ATK]   = 188;
$Equipment::Weapons[Slashing, %i, Delay] = 2.5;

%i++;
$Equipment::Weapons[Slashing, %i, Name]  = "Anchet's Sword";
$Equipment::Weapons[Slashing, %i, Skill] = 600;
$Equipment::Weapons[Slashing, %i, RL]    = 2;
$Equipment::Weapons[Slashing, %i, ATK]   = 265;
$Equipment::Weapons[Slashing, %i, Delay] = 4.133;

%i++;
$Equipment::Weapons[Slashing, %i, Name]  = "Keldrinite Long Sword";
$Equipment::Weapons[Slashing, %i, Skill] = 1120;
$Equipment::Weapons[Slashing, %i, RL]    = 1;
$Equipment::Weapons[Slashing, %i, ATK]   = 90;
$Equipment::Weapons[Slashing, %i, Delay] = 1;

// Bludgeoning

%i = 0;
$Equipment::Weapons[Bludgeoning, %i, Name]  = "Club";
$Equipment::Weapons[Bludgeoning, %i, Skill] = 0;
$Equipment::Weapons[Bludgeoning, %i, ATK]   = 12;
$Equipment::Weapons[Bludgeoning, %i, Delay] = 1;

%i++;
$Equipment::Weapons[Bludgeoning, %i, Name]  = "Cracked Club";
$Equipment::Weapons[Bludgeoning, %i, Skill] = 0;
$Equipment::Weapons[Bludgeoning, %i, ATK]   = 8;
$Equipment::Weapons[Bludgeoning, %i, Delay] = 1.5;

%i++;
$Equipment::Weapons[Bludgeoning, %i, Name]  = "Spiked Club";
$Equipment::Weapons[Bludgeoning, %i, Skill] = 60;
$Equipment::Weapons[Bludgeoning, %i, ATK]   = 30;
$Equipment::Weapons[Bludgeoning, %i, Delay] = 1;

%i++;
$Equipment::Weapons[Bludgeoning, %i, Name]  = "Cracked Spiked Club";
$Equipment::Weapons[Bludgeoning, %i, Skill] = 60;
$Equipment::Weapons[Bludgeoning, %i, ATK]   = 21;
$Equipment::Weapons[Bludgeoning, %i, Delay] = 1.5;

%i++;
$Equipment::Weapons[Bludgeoning, %i, Name]  = "Mace";
$Equipment::Weapons[Bludgeoning, %i, Skill] = 140;
$Equipment::Weapons[Bludgeoning, %i, ATK]   = 78;
$Equipment::Weapons[Bludgeoning, %i, Delay] = 2;

%i++;
$Equipment::Weapons[Bludgeoning, %i, Name]  = "Hammer Pick";
$Equipment::Weapons[Bludgeoning, %i, Skill] = 300;
$Equipment::Weapons[Bludgeoning, %i, ATK]   = 80;
$Equipment::Weapons[Bludgeoning, %i, Delay] = 1.666;

%i++;
$Equipment::Weapons[Bludgeoning, %i, Name]  = "War Hammer";
$Equipment::Weapons[Bludgeoning, %i, Skill] = 768;
$Equipment::Weapons[Bludgeoning, %i, ATK]   = 176;
$Equipment::Weapons[Bludgeoning, %i, Delay] = 2.666;

%i++;
$Equipment::Weapons[Bludgeoning, %i, Name]  = "War Maul";
$Equipment::Weapons[Bludgeoning, %i, Skill] = 900;
$Equipment::Weapons[Bludgeoning, %i, ATK]   = 175;
$Equipment::Weapons[Bludgeoning, %i, Delay] = 2.333;

%i++;
$Equipment::Weapons[Bludgeoning, %i, Name]  = "Quarter Staff";
$Equipment::Weapons[Bludgeoning, %i, Skill] = 20;
$Equipment::Weapons[Bludgeoning, %i, ATK]   = 35;
$Equipment::Weapons[Bludgeoning, %i, Delay] = 1.666;

%i++;
$Equipment::Weapons[Bludgeoning, %i, Name]  = "Long Staff";
$Equipment::Weapons[Bludgeoning, %i, Skill] = 620;
$Equipment::Weapons[Bludgeoning, %i, ATK]   = 114;
$Equipment::Weapons[Bludgeoning, %i, Delay] = 2;

%i++;
$Equipment::Weapons[Bludgeoning, %i, Name]  = "Justice Staff";
$Equipment::Weapons[Bludgeoning, %i, Skill] = 834;
$Equipment::Weapons[Bludgeoning, %i, ATK]   = 118;
$Equipment::Weapons[Bludgeoning, %i, Delay] = 1.666;

%i++;
$Equipment::Weapons[Bludgeoning, %i, Name]  = "Bone Club";
$Equipment::Weapons[Bludgeoning, %i, Skill] = 45;
$Equipment::Weapons[Bludgeoning, %i, ATK]   = 34;
$Equipment::Weapons[Bludgeoning, %i, Delay] = 1.333;

%i++;
$Equipment::Weapons[Bludgeoning, %i, Name]  = "Spiked Bone Club";
$Equipment::Weapons[Bludgeoning, %i, Skill] = 450;
$Equipment::Weapons[Bludgeoning, %i, ATK]   = 70;
$Equipment::Weapons[Bludgeoning, %i, Delay] = 1.333;

%i++;
$Equipment::Weapons[Bludgeoning, %i, Name]  = "Der OberHammer(tm)";
$Equipment::Weapons[Bludgeoning, %i, Skill] = 650;
$Equipment::Weapons[Bludgeoning, %i, RL]    = 1;
$Equipment::Weapons[Bludgeoning, %i, ATK]   = 230;
$Equipment::Weapons[Bludgeoning, %i, Delay] = 3.666;

%i++;
$Equipment::Weapons[Bludgeoning, %i, Name]  = "Thorr's Hammer";
$Equipment::Weapons[Bludgeoning, %i, Skill] = 990;
$Equipment::Weapons[Bludgeoning, %i, RL]    = 2;
$Equipment::Weapons[Bludgeoning, %i, ATK]   = 184;
$Equipment::Weapons[Bludgeoning, %i, Delay] = 3.166;

// Piercing

%i = 0;
$Equipment::Weapons[Piercing, %i, Name]  = "Knife";
$Equipment::Weapons[Piercing, %i, Skill] = 0;
$Equipment::Weapons[Piercing, %i, ATK]   = 18;
$Equipment::Weapons[Piercing, %i, Delay] = 1;

%i++;
$Equipment::Weapons[Piercing, %i, Name]  = "Rusty Knife";
$Equipment::Weapons[Piercing, %i, Skill] = 0;
$Equipment::Weapons[Piercing, %i, ATK]   = 13;
$Equipment::Weapons[Piercing, %i, Delay] = 1;

%i++;
$Equipment::Weapons[Piercing, %i, Name]  = "Dagger";
$Equipment::Weapons[Piercing, %i, Skill] = 60;
$Equipment::Weapons[Piercing, %i, ATK]   = 23;
$Equipment::Weapons[Piercing, %i, Delay] = 1;

%i++;
$Equipment::Weapons[Piercing, %i, Name]  = "Short Sword";
$Equipment::Weapons[Piercing, %i, Skill] = 140;
$Equipment::Weapons[Piercing, %i, ATK]   = 50;
$Equipment::Weapons[Piercing, %i, Delay] = 1.666;

%i++;
$Equipment::Weapons[Piercing, %i, Name]  = "Rusty Short Sword";
$Equipment::Weapons[Piercing, %i, Skill] = 140;
$Equipment::Weapons[Piercing, %i, ATK]   = 35;
$Equipment::Weapons[Piercing, %i, Delay] = 2.5;

%i++;
$Equipment::Weapons[Piercing, %i, Name]  = "Spear";
$Equipment::Weapons[Piercing, %i, Skill] = 280;
$Equipment::Weapons[Piercing, %i, ATK]   = 78;
$Equipment::Weapons[Piercing, %i, Delay] = 2;

%i++;
$Equipment::Weapons[Piercing, %i, Name]  = "Gladius";
$Equipment::Weapons[Piercing, %i, Skill] = 450;
$Equipment::Weapons[Piercing, %i, ATK]   = 80;
$Equipment::Weapons[Piercing, %i, Delay] = 1.666;

%i++;
$Equipment::Weapons[Piercing, %i, Name]  = "Trident";
$Equipment::Weapons[Piercing, %i, Skill] = 620;
$Equipment::Weapons[Piercing, %i, ATK]   = 114;
$Equipment::Weapons[Piercing, %i, Delay] = 2;

%i++;
$Equipment::Weapons[Piercing, %i, Name]  = "Rapier";
$Equipment::Weapons[Piercing, %i, Skill] = 768;
$Equipment::Weapons[Piercing, %i, ATK]   = 110;
$Equipment::Weapons[Piercing, %i, Delay] = 1.666;

%i++;
$Equipment::Weapons[Piercing, %i, Name]  = "Awl Pike";
$Equipment::Weapons[Piercing, %i, Skill] = 900;
$Equipment::Weapons[Piercing, %i, ATK]   = 200;
$Equipment::Weapons[Piercing, %i, Delay] = 2.666;

%i++;
$Equipment::Weapons[Piercing, %i, Name]  = "Pick Axe";
$Equipment::Weapons[Piercing, %i, Skill] = 0;
$Equipment::Weapons[Piercing, %i, ATK]   = 16;
$Equipment::Weapons[Piercing, %i, Delay] = 1.333;

%i++;
$Equipment::Weapons[Piercing, %i, Name]  = "Rusty Pick Axe";
$Equipment::Weapons[Piercing, %i, Skill] = 0;
$Equipment::Weapons[Piercing, %i, ATK]   = 11;
$Equipment::Weapons[Piercing, %i, Delay] = 22;

// Archery

%i = 0;
$Equipment::Weapons[Archery, %i, Name] = "Sling";
$Equipment::Weapons[Archery, %i, Skill] = 0;
$Equipment::Weapons[Archery, %i, ATK]   = 11;
$Equipment::Weapons[Archery, %i, Delay] = 1.5;

%i++;
$Equipment::Weapons[Archery, %i, Name] = "Short Bow";
$Equipment::Weapons[Archery, %i, Skill] = 25;
$Equipment::Weapons[Archery, %i, ATK]   = 23;
$Equipment::Weapons[Archery, %i, Delay] = 1;

%i++;
$Equipment::Weapons[Archery, %i, Name] = "Cracked Short Bow";
$Equipment::Weapons[Archery, %i, Skill] = 25;
$Equipment::Weapons[Archery, %i, ATK]   = 16;
$Equipment::Weapons[Archery, %i, Delay] = 1.5;

%i++;
$Equipment::Weapons[Archery, %i, Name] = "Long Bow";
$Equipment::Weapons[Archery, %i, Skill] = 318;
$Equipment::Weapons[Archery, %i, ATK]   = 86;
$Equipment::Weapons[Archery, %i, Delay] = 1.666;

%i++;
$Equipment::Weapons[Archery, %i, Name] = "Elven Bow";
$Equipment::Weapons[Archery, %i, Skill] = 685;
$Equipment::Weapons[Archery, %i, ATK]   = 89;
$Equipment::Weapons[Archery, %i, Delay] = 1;

%i++;
$Equipment::Weapons[Archery, %i, Name] = "Composite Bow";
$Equipment::Weapons[Archery, %i, Skill] = 438;
$Equipment::Weapons[Archery, %i, ATK]   = 85;
$Equipment::Weapons[Archery, %i, Delay] = 1.333;

%i++;
$Equipment::Weapons[Archery, %i, Name] = "Light Crossbow";
$Equipment::Weapons[Archery, %i, Skill] = 160;
$Equipment::Weapons[Archery, %i, ATK]   = 72;
$Equipment::Weapons[Archery, %i, Delay] = 2;

%i++;
$Equipment::Weapons[Archery, %i, Name] = "Cracked Light Crossbow";
$Equipment::Weapons[Archery, %i, Skill] = 160;
$Equipment::Weapons[Archery, %i, ATK]   = 50;
$Equipment::Weapons[Archery, %i, Delay] = 3;

%i++;
$Equipment::Weapons[Archery, %i, Name] = "Heavy Crossbow";
$Equipment::Weapons[Archery, %i, Skill] = 925;
$Equipment::Weapons[Archery, %i, ATK]   = 300;
$Equipment::Weapons[Archery, %i, Delay] = 2.666;

%i++;
$Equipment::Weapons[Archery, %i, Name] = "Repeating Crossbow";
$Equipment::Weapons[Archery, %i, Skill] = 550;
$Equipment::Weapons[Archery, %i, ATK]   = 75;
$Equipment::Weapons[Archery, %i, Delay] = 1;

%i++;
$Equipment::Weapons[Archery, %i, Name] = "Aeolus Wing";
$Equipment::Weapons[Archery, %i, Skill] = 805;
$Equipment::Weapons[Archery, %i, ATK]   = 101;
$Equipment::Weapons[Archery, %i, Delay] = 1;





// Items to go:

// $SkillRestriction[BluePotion] = $SkillHealing @ " 0";
// $SkillRestriction[CrystalBluePotion] = $SkillHealing @ " 0";
// $SkillRestriction[ApprenticeRobe] = $SkillEndurance @ " 0 " @ $SkillEnergy @ " 8";
// $SkillRestriction[LightRobe] = $SkillEndurance @ " 3 " @ $SkillEnergy @ " 80";
// $SkillRestriction[FineRobe] = $SkillEndurance @ " 9 " @ $SkillEnergy @ " 175";
// $SkillRestriction[BloodRobe] = $SkillEndurance @ " 8 " @ $SkillEnergy @ " 300";
// $SkillRestriction[AdvisorRobe] = $SkillEndurance @ " 10 " @ $SkillEnergy @ " 450";
// $SkillRestriction[ElvenRobe] = $SkillEndurance @ " 12 " @ $SkillEnergy @ " 620";
// $SkillRestriction[RobeOfVenjance] = $SkillEndurance @ " 18 " @ $SkillEnergy @ " 800";
// $SkillRestriction[PhensRobe] = $SkillEndurance @ " 20 " @ $SkillEnergy @ " 980";
// $SkillRestriction[QuestMasterRobe] = $MinAdmin @ " 3";

// $SkillRestriction[PaddedArmor] = $SkillEndurance @ " 5";
// $SkillRestriction[LeatherArmor] = $SkillEndurance @ " 40";
// $SkillRestriction[StuddedLeather] = $SkillEndurance @ " 95";
// $SkillRestriction[SpikedLeather] = $SkillEndurance @ " 135";
// $SkillRestriction[HideArmor] = $SkillEndurance @ " 180";
// $SkillRestriction[ScaleMail] = $SkillEndurance @ " 240";
// $SkillRestriction[BrigandineArmor] = $SkillEndurance @ " 300";
// $SkillRestriction[ChainMail] = $SkillEndurance @ " 350";
// $SkillRestriction[RingMail] = $SkillEndurance @ " 410";
// $SkillRestriction[BandedMail] = $SkillEndurance @ " 490";
// $SkillRestriction[SplintMail] = $SkillEndurance @ " 580";
// $SkillRestriction[BronzePlateMail] = $SkillEndurance @ " 660";
// $SkillRestriction[PlateMail] = $SkillEndurance @ " 775";
// $SkillRestriction[FieldPlateArmor] = $SkillEndurance @ " 840";
// $SkillRestriction[DragonMail] = $SkillEndurance @ " 950";
// $SkillRestriction[FullPlateArmor] = $SkillEndurance @ " 1065";
// $SkillRestriction[CheetaursPaws] = $MinLevel @ " 8";
// $SkillRestriction[BootsOfGliding] = $MinLevel @ " 25";
// $SkillRestriction[WindWalkers] = $MinLevel @ " 60";
// $SkillRestriction[KeldrinArmor] = $SkillEndurance @ " 1305";

// $SkillRestriction[KnightShield] = $SkillEndurance @ " 140";
// $SkillRestriction[HeavenlyShield] = $SkillEndurance @ " 540 " @ $SkillEnergy @ " 850";
// $SkillRestriction[DragonShield] = $SkillEndurance @ " 715";

// $SkillRestriction[SmallRock] = $SkillArchery @ " 0";
// $SkillRestriction[BasicArrow] = $SkillArchery @ " 0";
// $SkillRestriction[ShortQuarrel] = $SkillArchery @ " 0";
// $SkillRestriction[LightQuarrel] = $SkillArchery @ " 0";
// $SkillRestriction[SheafArrow] = $SkillArchery @ " 0";
// $SkillRestriction[StoneFeather] = $SkillArchery @ " 0";
// $SkillRestriction[BladedArrow] = $SkillArchery @ " 0";
// $SkillRestriction[HeavyQuarrel] = $SkillArchery @ " 0";
// $SkillRestriction[MetalFeather] = $SkillArchery @ " 0";
// $SkillRestriction[Talon] = $SkillArchery @ " 0";
// $SkillRestriction[CeraphumsFeather] = $SkillArchery @ " 0";