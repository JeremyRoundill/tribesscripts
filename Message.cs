// Message.cs

function Message::print(%message, %delay) {
	Client::centerPrint(%message, 1);
	if(%delay > 0) {
		Schedule::Add("Client::centerPrint(\"\", 1);", %delay);
	} else {
		Schedule::Cancel("Client::centerPrint(\"\", 1);");
	}
}

function Message::clear() {
	Client::centerPrint("", 1);	
	Schedule::Cancel("Client::centerPrint(\"\", 1);");	
}