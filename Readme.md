# Joundill's script pack!

## Installation

### 1. Requirements

* Bov's plugin pack
* Z rotation plugin
* DeusRPG Pack
* Presto Pack

#### Z Rotation plugin

You need to get a copy of my RotPlugin.dll, which provides a function `getZRotation();` in client-side Tribesscript. This is what's used to allow us to turn for automatic pathing.

To install it, place it in /Plugins and modify /Plugins/Scripts/PluginLoader.cs to include this line:

```
$PluginLoader::RotPlugin = true;
```

### 2. Extracting scripts

These scripts should be placed in /config/Jeremy

Add the following line to your autoexec.cs:

```
exec("Jeremy\\Jeremy.cs");
```

## Usage

You can press Shift + J to open the menu.

Shift + 0 stops auto casting.



## Roadmap

* Upgrade SP spender to max out skills, or hit required levels (i.e. Neutral Casting 200) instead of just spending a constant amount
* Trigger event on level up
* Trigger event on SP level up

## Bugs to fix

* Receiving a party invite while spending SP will cause the tab menu to close prematurely, breaking the SP spend(y)