// Actions.cs


// Spell cooldowns
// NB: cooldown definitely decreases with casting level. DaThief said so
// worth looking through the trpg code to find the calculation
// Offensive
$Actions::spellDelay[thorn] = 1;
$Actions::spellDelay[fireball] = 2.1; 
$Actions::spellDelay[firebomb] = 4.2; // guess 
$Actions::spellDelay[icespike] = 2.5; // guess 
$Actions::spellDelay[icestorm] = 3.3; // guess 
$Actions::spellDelay[ironfist] = 14.5; // guess 
$Actions::spellDelay[cloud] = 4.2; // guess 
$Actions::spellDelay[melt] = 4.2;  // 3.7 worked - maybe cooldown decreases? 
$Actions::spellDelay[powercloud] = 7;
$Actions::spellDelay[hellstorm] = 16.5; // guess 
$Actions::spellDelay[beam] = 15; // guess 
$Actions::spellDelay[dimensionrift] = 21;


// From base RPG server-side:
// %skt = $SkillType[$Spell::keyword[%i]];
// %sk1 = $PlayerSkill[%clientId, %skt];
// %gsa = GetSkillAmount($Spell::keyword[%i], %skt);
// %sk2 = %sk1 - %gsa;
// %sk = Cap(%sk2, 0, "inf");
// %rt = $Spell::recoveryTime[%i];
// %a = %rt / 2;
// %b = (1000 - %sk) / 1000;
// %c = %b * %a;
// %recovTime = $Spell::delay[%i] + Cap(%a + %c, %a, %rt);    //recovery time is never smaller than half of the original and never bigger than the original.

// Heals
$Actions::spellDelay[heal] = 4;

$Actions::mediCastingSpell = "";

// Do an action
function Actions::doAction(%actionName) {
    remoteeval(2048, say(0, "#" @ %actionName));
}

// Cast spell
function Actions::castSpell(%spellName) {
    remoteeval(2048, say(0, "#cast " @ %spellName));
}

// Schedule an action for later
function Actions::scheduleAction(%actionName, %delay) {
    Schedule::Add("Actions::doAction(\"" @ %actionName @ "\");", %delay);
}

function Actions::cancelAction(%actionName) {
    Schedule::Cancel("Actions::doAction(\"" @ %actionName @ "\");");
}

// Schedule a spell for later
function Actions::scheduleSpell(%spellName, %delay) {
    Actions::scheduleAction("cast " @ %spellName, %delay);
}

function Actions::cancelSpell(%spellName) {
    Actions::cancelAction("cast " @ %spellName);
}

// Begin meditation + casting
function Actions::startMediCast(%spellName) {

    $Actions::mediCastingSpell = %spellName;
    if($Actions::mediCasting) {
        if($Actions::mediCastingSpell == %spellName) {
            Message::print("<jc>You're already autocasting " @ %spellName @ "!", 5);
            return;
        }

        Message::print("<jc>You're already autocasting " @ $Actions::mediCastingSpell @ ". Changing spell to " @ %spellName, 5);
        return;
    }

    if($Actions::spellDelay[%spellName] == "") {
        Message::print("<jc>The delay for that spell isn't coded in to Actions.cs!", 5);
        return;        
    }
    
    Message::print("<jc><f0>Autocasting <f1>" @ %spellName, 5);
    Actions::mediCasting();
}

// Stop meditation + casting
function Actions::stopMediCast() {
    Message::print("<jc><f0>Stopping autocast", 5);
    $Actions::mediCastingSpell = "";
    Actions::cancelAction("meditate");
    Actions::cancelAction("wake");
    Schedule::Cancel("Actions::mediCasting();");
    Actions::scheduleAction("wake", 1);
}

// Register unknown spell and cast
function Actions::registerAndCast(%spellName, %delay) {
    $Actions::spellDelay[%spellName] = %delay;
    Actions::startMedicast(%spellName);
}

// Meditation + casting
function Actions::mediCasting() {
    if($Actions::spellDelay[$Actions::mediCastingSpell] == "") {
        echo("ERROR: Actions::medicasting doesn't have a delay for this spell");
        return;
    }

    %spellDelay = $Actions::spellDelay[$Actions::mediCastingSpell];
    // Do the spell
    Actions::castSpell($Actions::mediCastingSpell);

    // Don't meditate + wake if the spell cooldown is too short
    if(%spellDelay > 3) {
        // Meditate 1s after spell is cast
        Actions::scheduleAction("meditate", 1);
        // Wake 1s before spell cooldown ends
        Actions::scheduleAction("wake", %spellDelay - 1);
    }
    // Rinse and repeat recursively when spell cooldown ends
    Schedule::Add("Actions::mediCasting();", %spellDelay);
}

function Actions::startAutoAttack() {
    postAction(2048, IDACTION_BREAK1, 1);
    schedule("postAction(2048, IDACTION_FIRE1, 1);", 0.5);    
}


// other spells

// SkillRestriction[teleport] = $SkillNeutralCasting @ " 60";
// $SkillRestriction[transport] = $SkillNeutralCasting @ " 200";
// $SkillRestriction[advtransport] = $SkillNeutralCasting @ " 350";
// $SkillRestriction[remort] = $SkillNeutralCasting @ " 0 " @ $MinLevel @ " 101";
// $SkillRestriction[mimic] = $SkillNeutralCasting @ " 145 " @ $MinRemort @ " 2";
// $SkillRestriction[masstransport] = $SkillNeutralCasting @ " 650 " @ $MinRemort @ " 1";

// $SkillRestriction[heal] = $SkillDefensiveCasting @ " 10";
// $SkillRestriction[advheal1] = $SkillDefensiveCasting @ " 80";
// $SkillRestriction[advheal2] = $SkillDefensiveCasting @ " 110";
// $SkillRestriction[advheal3] = $SkillDefensiveCasting @ " 200";
// $SkillRestriction[advheal4] = $SkillDefensiveCasting @ " 320";
// $SkillRestriction[advheal5] = $SkillDefensiveCasting @ " 400";
// $SkillRestriction[advheal6] = $SkillDefensiveCasting @ " 500";
// $SkillRestriction[godlyheal] = $SkillDefensiveCasting @ " 600";
// $SkillRestriction[fullheal] = $SkillDefensiveCasting @ " 750";
// $SkillRestriction[massheal] = $SkillDefensiveCasting @ " 850 " @ $MinRemort @ " 2";
// $SkillRestriction[massfullheal] = $SkillDefensiveCasting @ " 950 " @ $MinRemort @ " 3";
// $SkillRestriction[shield] = $SkillDefensiveCasting @ " 20";
// $SkillRestriction[advshield1] = $SkillDefensiveCasting @ " 60";
// $SkillRestriction[advshield2] = $SkillDefensiveCasting @ " 140";
// $SkillRestriction[advshield3] = $SkillDefensiveCasting @ " 290";
// $SkillRestriction[advshield4] = $SkillDefensiveCasting @ " 420";
// $SkillRestriction[advshield5] = $SkillDefensiveCasting @ " 635";
// $SkillRestriction[massshield] = $SkillDefensiveCasting @ " 680";